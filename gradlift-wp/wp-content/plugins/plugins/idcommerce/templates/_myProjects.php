<li class="myprojects">
	<div title="Project Thumbnail" class="project-item project-thumb"><div class="image" style="<?php echo (!empty($thumb) ? 'background-image: url('.$thumb.');' : ''); ?>"></div></div>
	<div title="Project Name" class="project-item project-name"><?php echo get_the_title($post_id); ?></div>
	<div class="project-item option-list">
		<a title="Edit Project" href="<?php echo md_get_durl().$prefix; ?>edit_project=<?php echo $post_id; ?>"><i class="fa fa-edit"></i></a>
		<a title="Upload File" href="<?php echo md_get_durl().$prefix; ?>project_files=<?php echo $post_id; ?>"><i class="fa fa-cloud-upload"></i></a>
		<a title="View Project" href="<?php echo $permalink; ?>"><i class="fa fa-eye"></i></a>
		<a title="Export Orders" href="<?php echo md_get_durl().$prefix; ?>export_project=<?php echo $post_id; ?>"><i class="fa fa-file-excel-o"></i></a>
	</div>
	<div title="Project Status" class="project-item project-status"><?php echo (strtoupper($status) == 'PUBLISH' ? __('PUBLISHED', 'memberdeck') : $status); ?></div>
</li>