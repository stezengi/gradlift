<?php
class bbSocialShare {

	protected $url = '';
	protected $img = '';
	protected $title = '';
	protected $desc = '';
	
	public $useFontAwesome = true;
	public $beforeLink = '<li class="bb-social-share__item">';
	public $linkClass = 'bb-social-share__link';
	public $afterLink = '</li>';
	public $beforeList = '<ul class="bb-social-share__list">';
	public $afterList = '</ul>';
	public $beforeDiv = '<div class="bb-social-share">';
	public $afterDiv = '</div>';
	private $textBefore = null;
	private $textAfter = null;
	
	private $networks = array ( );
	
	
	//Facebook variables
	private $facebook = 'http://www.facebook.com/sharer.php?u={url}';
	public $facebookIcon = 'facebook';
	public $app_id  = '';
	public $redirect_url = '';
	private $facebookDialog = 'https://www.facebook.com/dialog/share?app_id={app_id}&display=page&href={url}&redirect_uri={redirect_url}';
	
	//Twitter
	//full twitter sharer: https://twitter.com/share?url={url}&text={title}&via={via}&hashtags={hashtags}
	private $twitter = 'https://twitter.com/share?url={url}&text={title}';
	public $twitterIcon = 'twitter';
	public $via = '';
	public $hashtags = '';
	
	//Google
	private $google = 'https://plus.google.com/share?url={url}';
	public $googleIcon = 'google';
	
	
	//Pinterest
	private $pinterest = 'https://pinterest.com/pin/create/bookmarklet/?media={img}&url={url}&description={title}';
	public $pinterestIcon = 'pinterest';
	public $is_video = '';
	
	private $linkedin = 'http://www.linkedin.com/shareArticle?url={url}&title={title}';
	public $linkedinIcon = 'linkedin';
	
	private $tumblr = 'http://www.tumblr.com/share/link?url={url}&name={title}&description={desc}';
	public $tumblrIcon = 'tumblr';
	
	private $buffer = 'http://bufferapp.com/add?text={title}&url={url}';
	public $bufferIcon = 'bars';
	
	private $digg = 'http://digg.com/submit?url={url}&title={title}';
	public $diggIcon = 'digg';
	
	private $reddit = 'http://reddit.com/submit?url={url}&title={title}';
	public $redditIcon = 'reddit';
	
	private $stumbleupon = 'http://www.stumbleupon.com/submit?url={url}&title={title}';
	public $stumbleuponIcon = 'stumbleupon';
	
	private $delicious = 'https://delicious.com/save?v=5&provider={provider}&noui&jump=close&url={url}&title={title}';
	public $deliciousIcon = 'delicious';
	
	public function __construct()
	{
		$networks = get_theme_mod( 'bbss_networks' );
		$this->set_networks( $networks );
		$this->set_text_before ( get_theme_mod( 'bbss_text_before' ) );
	}
	
	public function set_vars ( $atts = null )
	//$url = null, $title = null, $img = null, $desc = null, $hashtags = null, $via = null, $is_video = false ) 
	{
		extract($atts);
		$this->set_url($url);
		$this->title = $title;
		$this->img = $img;
		$this->desc = $desc;
	}
	public function set_networks ( $array = null ) 
	{
		if( is_array( $array ) ) {
			$this->networks = $array;
		} elseif( $array ) {
			$this->networks = array( $array );
		}
	}
	private function set_url ( $url = null ) 
	{
		//$this->url = urlencode( $url );
		$this->url = urlencode( wp_get_shortlink() );
	}
	private function set_text_before ( $textBefore = null ) 
	{
		$this->textBefore = '<div class="bb-social-share__text-before">' . $textBefore . '</div>';
	}
 
	private function set_text_after ( $textAfter = null ) 
	{
		$this->textAfter = '<div class="bb-social-share__text-after">' . $textAfter . '</div>';
	}
 
	
	private function facebook ()
	{
		if(	$this->app_id ) {
			$url = $this->facebookDialog;
		} else {
			$url = $this->facebook;
		}
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->facebookIcon, 'Facebook' );
		return $url;	
	}
	
	private function twitter ()
	{
		//https://twitter.com/share?url={url}&text={title}&via={via}&hashtags={hashtags}
		$url = $this->twitter ;
		if( $this->via ) {
			$url .= '&via={via}';
		}
		if( $this->hashtags ) {
			$url .= '&hashtags={hashtags}';
		}
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->twitterIcon, 'Twitter' );
		return $url;		
	}
	
	private function pinterest ()
	{
		//https://pinterest.com/pin/create/bookmarklet/?media={img}&url={url}&description={title}&is_video={is_video}';
		$url = $this->pinterest ;
		if( $this->is_video ) {
			$url .= '&is_video={is_video}';
		}
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->pinterestIcon, 'Pinterest' );
		return $url;		
	}
	
	private function google ()
	{
		$url = $this->google;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->googleIcon, 'Google' );
		return $url;	
	}
	
	private function tumblr ()
	{
		$url = $this->tumblr;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->tumblrIcon, 'Tumblr' );
		return $url;	
	}
	private function tumbler()
	{
		return $this->tumblr();
	}
	
	private function linkedin ()
	{
		$url = $this->linkedin;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->linkedinIcon, 'LinkedIn' );
		return $url;	
	}
	
	private function stumbleupon ()
	{
		$url = $this->stumbleupon;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->stumbleuponIcon, 'StumbleUpon' );
		return $url;	
	}
	
	private function delicious ()
	{
		$url = $this->delicious;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->deliciousIcon, 'Delicious' );
		return $url;	
	}
	
	private function reddit ()
	{
		$url = $this->reddit;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->redditIcon, 'Reddit' );
		return $url;	
	}
	
	private function digg ()
	{
		$url = $this->digg;
		$url = $this->replace_variables( $url );
		$url = $this->wrap_url( $url, $this->diggIcon, 'Digg' );
		return $url;	
	}
	
	private function replace_variables( $url ) 
	{
		$url = str_replace ( '{url}', $this->url, $url );
		$url = str_replace ( '{title}', $this->title, $url );
		$url = str_replace ( '{desc}', $this->desc, $url );
		$url = str_replace ( '{img}', $this->img, $url );
		$url = str_replace ( '{hashtags}', $this->hashtags, $url );
		$url = str_replace ( '{via}', $this->via, $url );
		$url = str_replace ( '{app_id}', $this->app_id, $url );
		$url = str_replace ( '{redirect_url}', $this->redirect_url, $url );
		$url = str_replace ( '{is_video}', $this->is_video, $url );
		
		return $url;
	}
	
	private function wrap_url ($url = null, $icon = null, $linkText = null ) 
	{
		$link = $this->beforeLink . '<a href="'.$url.'" class="' . $this->linkClass . '" target="_blank" >';
		if( $this->useFontAwesome ) $link .= '<i class="fa fa-' . $icon . '"></i>';
		$link .= '<span>'. $linkText . '</span></a>' . $this->afterLink;
		return $link;
	}
	
	//OLD function do not use - Use bb_social_share_list
	public function bear_bones_social_share_list ( $atts = null ) 
	{
		echo bb_social_share_list( $atts );
	}
	
	
	public function bb_social_share_list( $atts = null ) {
		
		if( is_array( $atts) ) {
			extract($atts);
		} else {
			global $post;
			//prar( $post->ID );
			$title = get_the_title( $post->ID ); 
			$url = get_the_permalink( $post->ID ); 
			$image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
			if ( function_exists( 'bb_excerpt' ) ) {
				$excerpt = bb_excerpt( null, $post->ID, null );
			} else {
				$excerpt = get_the_excerpt( $post->ID );
			}
		}
		
		if( isset( $url ) ) $this->set_url($url);
		if( isset( $title ) ) $this->title = $title;
		if( isset( $image ) ) $this->img = $image;
		if( isset( $img ) ) $this->img = $img;
		if( isset( $excerpt ) ) $this->desc = $excerpt;
		if( isset( $desc ) ) $this->desc = $excerpt;
		if( isset( $networks ) ) $this->set_networks( $networks );

		$socialShareList = $this->beforeDiv;
		$socialShareList .= $this->textBefore;
		$socialShareList .= $this->beforeList;
		foreach ( $this->networks as $network ) {
			if( method_exists( get_class(), $network ) ) $socialShareList .=  $this->$network();
		}
		$socialShareList .= $this->afterList;
		$socialShareList .= $this->textAfter;
		$socialShareList .= $this->afterDiv;
		return $socialShareList;

	}
}
?>