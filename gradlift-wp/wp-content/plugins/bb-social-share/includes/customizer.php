<?php
add_action('customize_register', 'bear_bones_social_share_customize_register');
function bear_bones_social_share_customize_register($wp_customize) {
	/*------------------------------------*\
		Social Share
	\*------------------------------------*/
	
	$wp_customize->add_section( 'bear_bones_social_share_options', array(
			'title'          => __( 'Social Share Options', 'bearbones' ),
			'priority'       => 300,
		) );
	
	
	$wp_customize->add_setting( 'bbss_networks', array(
		'default' => null // Either empty or fill it with your default values
	) );
	class Bear_Bones_Social_Customize_Control_Multiple_Select extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 */
		public $type = 'multiple-select';

		/**
		 * Displays the multiple select on the customize screen.
		 */
		public function render_content() {

		if ( empty( $this->choices ) )
			return;
		?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<select <?php $this->link(); ?> multiple="multiple" style="height: 100%;">
					<?php
						foreach ( $this->choices as $value => $label ) {
							$selected = ( in_array( $value, $this->value() ) ) ? selected( 1, 1, false ) : '';
							echo '<option value="' . esc_attr( $value ) . '"' . $selected . '>' . $label . '</option>';
						}
					?>
				</select>
			</label>
		<?php }
		
		/*  USAGE *
		$wp_customize->add_setting( 'multiple_select_setting', array(
			'default' => array(), // Either empty or fill it with your default values
		) );
		 
		$wp_customize->add_control(
			new Bear_Bones_Customize_Control_Multiple_Select(
				$wp_customize,
				'multiple_select_setting',
				array(
					'settings' => 'multiple_select_setting',
					'label'    => 'Testing multiple select',
					'section'  => 'themedemo_demo_settings', // Enter the name of your own section
					'type'     => 'multiple-select', // The $type in our class
					'choices'  => array( 'google' => 'Google', 'bing' => 'Bing' ) // Your choices
				)
			)
		);
		*/
	}
	
	$wp_customize->add_control(
		new Bear_Bones_Social_Customize_Control_Multiple_Select(
			$wp_customize,
			'bbss_networks',
			array(
				'settings' => 'bbss_networks',
				'label'    => 'Networks to share with',
				'section'  => 'bear_bones_social_share_options', // Enter the name of your own section
				'type'     => 'multiple-select', // The $type in our class
				'priority' => 1,
				'choices'  => array( 
					'delicious' => 'Delicious',
					'digg' => 'Digg',
					'email' => 'Email',
					'facebook' => 'Facebook',
					'google' => 'Google',
					'linkedin' => 'LinkedIn',
					'pinterest' => 'Pinterest',
					'reddit' => 'Reddit',					
					'stumbleupon' => 'StumbleUpon',
					'tumblr' => 'Tumblr',					
					'twitter' => 'Twitter',					
				) // Your choices
			)
		)
	);
	$wp_customize->add_setting( 'bbss_custom_order', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_custom_order', array(
        'label'   => 'Custom order (comma separated)',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 4,
    ) );
	
	$wp_customize->add_setting( 'bbss_text_before', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_text_before', array(
        'label'   => 'Text to display before',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 5,
    ) );
	$wp_customize->add_setting( 'bbss_text_after', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_text_after', array(
        'label'   => 'Text to display after',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 6,
    ) );
	$wp_customize->add_setting( 'bbss_show_on_pages', array(
			'default'        => 0,
			'type'			 => 'theme_mod',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'bb_sanitize_checkbox',
		) );
	$wp_customize->add_control(	'bbss_show_on_pages',
			array(
				'type' => 'checkbox',
				'label' => 'Show on pages',
				'section' => 'bear_bones_social_share_options',
				'priority'       => 10,
			)
		);
	$wp_customize->add_setting( 'bbss_show_on_posts', array(
			'default'        => 0,
			'type'			 => 'theme_mod',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'bb_sanitize_checkbox',
		) );
	$wp_customize->add_control(	'bbss_show_on_posts',
			array(
				'type' => 'checkbox',
				'label' => 'Show on posts',
				'section' => 'bear_bones_social_share_options',
				'priority'       => 20,
			)
		);
	$wp_customize->add_setting( 'bbss_show_before_content', array(
			'default'        => 0,
			'type'			 => 'theme_mod',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'bb_sanitize_checkbox',
		) );
	$wp_customize->add_control(	'bbss_show_before_content',
			array(
				'type' => 'checkbox',
				'label' => 'Show before content',
				'section' => 'bear_bones_social_share_options',
				'priority'       => 30,
			)
		);
		
		
	$wp_customize->add_setting( 'bbss_include_twitter_handle', array(
			'default'        => 0,
			'type'			 => 'theme_mod',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'bb_sanitize_checkbox',
		) );
	$wp_customize->add_control(	'bbss_include_twitter_handle',
			array(
				'type' => 'checkbox',
				'label' => 'Include twitter handle',
				'section' => 'bear_bones_social_share_options',
				'priority'       => 40,
			)
		);
	$wp_customize->add_setting( 'bbss_twitter_handle', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_twitter_handle', array(
        'label'   => 'Twitter handle',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 45,
    ) );
	
	$wp_customize->add_setting( 'bbss_email_subject', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_email_subject', array(
        'label'   => 'Email subject',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 50,
    ) );
		$wp_customize->add_setting( 'bbss_email_body', array(
        'default'        => null,
		'type'			 => 'theme_mod',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'bbss_sanitize_text',
    ) );
 
    $wp_customize->add_control( 'bbss_email_body', array(
        'label'   => 'Email body',
        'section' => 'bear_bones_social_share_options',
        'type'    => 'text',
		'priority'       => 55,
    ) );
}

function bear_bones_social_share_style() {

	 ?>
		<style type="text/css">
			#customize-control-bbss_networks select {
				width: 100%;
				height: 15em!important;
			}
		</style>
	<?php 
	
}
add_action( 'admin_enqueue_scripts', 'bear_bones_social_share_style' );


if ( class_exists('WP_Customize_Control') && !class_exists( 'Bear_Bones_Customize_Control_Multiple_Select' ) ) {
//http://jayj.dk/multiple-select-lists-theme-customizer/
	/**
	 * Multiple select customize control class.
	 */
	
}

function bbss_sanitize_text( $input ) {
    return sanitize_text_field( $input );
}
?>