<?php



function translation_tab_fields() {
    $options = get_option('boxit_options');
    $fields = array(
        /* section */
        array(
            'id' => 'strings',
            'title' => 'BOXIT string translation',
            'fields' => array(
                /* fields */
                array(
                    'id' => 'trans',
                    'title' => ''
                )
            )
        )
    );
    return $fields;
}






function boxit_strings_section() {
    echo '<input type="hidden" name="boxit_tab" value="translation"/>';
    ?>
    <p>Use inputs in the right column to translate strings used by BOXIT on frontend and in email notifications.<br>
        Note: Some error messages like Dropbox API errors and upload script errors can't be translated.</p>
    <?php
}

function boxit_strings_trans() {
    $options = get_option('boxit_options');
    //echo "<pre>"; print_r($options['trans']); echo "</pre>";
    ?>
    <div style="margin-bottom:10px">
        <strong style="width:35%; display:inline-block;">Default strings:</strong>
        <span style="width:5.5%; display:inline-block; text-align:center"></span>
        <strong style="width:35%; display:inline-block;">Custom translation:</strong>
    </div>
    <?php
    $boxit_strings = get_boxit_strings(); /* definde in strings.php */
    translation_form($boxit_strings);
}





function translation_form($data) {
    $options = get_option('boxit_options');
    foreach ($data as $t) {
        $v = (isset($options['trans'][(string)$t[0]][1]))? $options['trans'][(string)$t[0]][1] : '';
        ?>
        <div style="margin-bottom:10px">
            <input type="text" name="boxit_options[trans][<?php echo $t[0] ?>][0]" value="<?php echo $t[1] ?>" readonly style="width:35%"/>
            <span style="width:5%; display:inline-block; text-align:center"> ----- </span>
            <input type="text" name="boxit_options[trans][<?php echo $t[0] ?>][1]" value="<?php echo $v; ?>" style="width:35%"/>
            <div class="boxit_explain"></div>
            <p><i>&uarr; <?php echo $t[2] ?></i></p>
        </div>
        <?php
    }
}