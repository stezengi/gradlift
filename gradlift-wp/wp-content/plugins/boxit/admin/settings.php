<?php

use \Dropbox as dbx;



// reset connection to Dropbox plugin
if(is_admin() && isset($_GET['reinstall_boxit'])) {
    delete_config_file();
    delete_option('dropbox_key_secret');
    delete_option('dropbox_auth_code');
    delete_option('dropbox_access_token');
    header('Location: '.admin_url('options-general.php?page=boxit_settings'));
}


//delete boxit settings
if(is_admin() && isset($_GET['drop_boxit_settings'])) {
	delete_option('boxit_options');
}



/**
 *
 * Default settings
 *
 */
$boxit_setup = array(

    /* GENERAL TAB */
    'user_folder' => 0,
    'name_folder' => 0,
    'only_for_logged_in' => 1,
    'name_email' => 0,
    'upload_message' => 0,
    'captcha' => 1,
    'size_limit' => '1000000', /* 1Mb */
    'chunk' => '1000000', /* 1Mb */
    'filetypes' => 'jpeg,jpg,gif,png',
    'email_notification' => 0,
    'notifications_address' => get_bloginfo( 'admin_email' ),
    'extra_notification' => 0,
    'debug_console' => 0,
    /* EMAILS TAB */
    'main_template' => '<p>New files were uploaded to Your dropbox.</p>
                        <p>Files uploaded by: {name}, {email}.</p>
                        <p>Upload message: {message}</p>
                        <p>{filelist}</p>',
    'main_template_subject' => 'new files uploaded using BOXIT',
    'extra_template' => '<p>Hi {name}!</p>
                        <p>You just uploaded the following files usiung BOXIT:</p>
                        <p>{filelist}</p>',
    'extra_template_subject' => 'new files uploaded using BOXIT',
    /* APPEARANCE */
    'primary_color' => '#03C4A6',
    'secondary_color' => '#C7C7C7',
    'text_on_primary' => '#ffffff',
    'hide_logo' => 0,
    'hide_space' => 0,
    /* translation */
    'trans' => array()

);



/**
 *
 * Save the default settings
 *
 */
if(!get_option('boxit_options')) {
    add_option('boxit_options', $boxit_setup);
}




/**
 *
 * Add settings page menu item
 *
 */
if ( is_admin() ){
    add_action( 'admin_menu', 'boxit_admin_menu' );
}



/**
 *
 * Add plugin settings page
 *
 */
function boxit_admin_menu(){
    add_options_page('BOXIT','BOXIT','manage_options','boxit_settings','boxit_settings_wrapper');
}




/**
 *
 * Create the tabs for the settings page
 * @param  string $current current/default  tab
 * @return HTML          The tab switcher
 *
 */
function boxit_settings_tabs( $current = 'general' ) {
    $tabs = array(
        'general' => 'General',
        'emails' => 'Emails',
        'appearance' => 'Appearance',
        'translation' => 'Translation'
        );
    echo '<h2 class="nav-tab-wrapper" style="margin-bottom:20px;">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=boxit_settings&tab=$tab'>$name</a>";
    }
    echo '</h2>';
}



function boxit_status() {
    if(!file_exists(dirname(dirname(__FILE__))."/app-info.json")) return false;
    $appInfo = dbx\AppInfo::loadFromJsonFile(dirname(dirname(__FILE__))."/app-info.json");
    $webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
    if(get_option('dropbox_access_token')) {
        $dbxClient = new dbx\Client(get_option('dropbox_access_token'), "PHP-Example/1.0");
        try {
            $accountInfo = $dbxClient->getAccountInfo();
        } catch (Exception $e) {
            //echo '<div style="margin-left:0px;" class="error"><p>Caught exception: '.$e->getMessage().'</p></div>';
        }
        if($accountInfo !== NULL) {
            return $accountInfo;
        } else {
            return false;
        }
    }
}





/**
 *
 * The settings wrappers (form)
 *
 */
function boxit_settings_wrapper() {
    if(!boxit_status()) {
        if(!boxit_install()) return;
    }
    ?>
    <div class="wrap">
        <?php
        //checking support for 64 bit integers - required by Dropbox API (made silent in dropbox-sdk/Dropbox/RequestUtil.php#22)
        if (strlen((string) PHP_INT_MAX) < 19) : ?>
            <div style="margin-left:0px; border-left:4px solid #FFCD28;" class="error">
                <p>Warning: You are using the 32-bit build of PHP (Dropbox SDK uses 64-bit integers). This could cause problems because some of the numbers used by Dropbox API (file sizes, quota, etc) can be larger than 32-bit ints can handle.</p>
            </div>
        <?php endif ?>
        <?php
            //display the tabs
            if ( isset ( $_GET['tab'] ) ) boxit_settings_tabs($_GET['tab']); else boxit_settings_tabs('general');
        ?>
        <form method="post" action="options.php" enctype="multipart/form-data">
            <?php
            settings_fields('boxit_options');
            do_settings_sections('boxit_plugin');
            submit_button();
            ?>
        </form>
    </div>
    <?php
}




/**
 *
 * Initialize the settings
 *
 */
if ( is_admin() ) {
    add_action('admin_init', 'boxit_admin_init');
}






/**
 *
 * Settings sections and fields setup
 *
 */
function boxit_admin_init(){
    register_setting( 'boxit_options', 'boxit_options', 'boxit_options_validate' );

    $tab = (isset($_GET['tab']))? $_GET['tab'] : 'general';

    $settings_setup = array();

    switch($tab) :
        case 'general' :
            require('general_tab.php');
            $settings_setup = general_tab_fields();
            break;
        case 'emails' :
            require('emails_tab.php');
            $settings_setup = emails_tab_fields();
            break;
        case 'appearance' :
            require('appearance_tab.php');
            $settings_setup = appearance_tab_fields();
            break;
        case 'translation' :
            require('translation_tab.php');
            $settings_setup = translation_tab_fields();
            break;
    endswitch;

    /*
    Add fileds from fields setttings array
    */
    foreach($settings_setup as $section) {
        add_settings_section('boxit_'.$section['id'], $section['title'], 'boxit_'.$section['id'].'_section', 'boxit_plugin');
        if(count($section['fields']) > 0) {
            foreach($section['fields'] as $field) {
                add_settings_field(
                    'boxit_'.$field['id'],
                    $field['title'],
                    'boxit_'.$section['id'].'_'.$field['id'],
                    'boxit_plugin',
                    'boxit_'.$section['id']
                );
            }
        }
    }
}






/**
 *
 * VALIDATE & PREPARE FOR SAVING
 *
 * Validates and PREPARES FOR SAVING the values from ALL the inputs
 * @param array $input The array that holds all the inputs from the settings page
 *
 * (the saving is handled by Wordpress)
 *
 */
function boxit_options_validate($input) {
    global $boxit_setup; //default settings array

    $options = get_option('boxit_options');

    $settings_tab = $_POST['boxit_tab'];

    /* GENERAL TAB */
    if($settings_tab == 'general') {

        $options['user_folder'] = $input['user_folder'];

        $options['name_folder'] = $input['name_folder'];

        $options['organize'] = $input['organize'];

        $options['only_for_logged_in'] = $input['only_for_logged_in'];

        $options['name_email'] = $input['name_email'];

        $options['upload_message'] = $input['upload_message'];

        $options['captcha'] = $input['captcha'];

        $options['size_limit'] = $input['size_limit'];

        $options['chunk'] = $input['chunk'];

        $options['filetypes'] = str_replace('.', '', str_replace(' ', '', strtolower(trim($input['filetypes']))));

        $options['hide_on_start'] = $input['hide_on_start'];

        $options['email_notification'] = $input['email_notification'];

        $options['notifications_address'] = $input['notifications_address'];

        $options['extra_notification'] = $input['extra_notification'];

        $options['thank_you'] = $input['thank_you'];

        $options['thankyou_page'] = trim($input['thankyou_page']);

        $options['debug_console'] = $input['debug_console'];
    }


    /* EMAILS TAB */
    if($settings_tab == 'emails') {

        $options['main_template'] = trim($_POST['boxit_main_template']);
        if($options['main_template'] == '') {
            $options['main_template'] = $boxit_setup['main_template'];
            add_settings_error('main_template','mt','Email templates can not be empty! - default templates were used.');
        }

        $options['main_template_subject'] = $input['main_template_subject'];

        $options['extra_template'] = trim($_POST['boxit_extra_template']);
        if($options['extra_template'] == '') {
            $options['extra_template'] = $boxit_setup['extra_template'];
            add_settings_error('extra_template','et','Email templates can not be empty! - default templates were used.');
        }

        $options['extra_template_subject'] = $input['extra_template_subject'];
    }


    if($settings_tab == 'appearance') {

        $options['primary_color'] = $input['primary_color'];

        $options['secondary_color'] = $input['secondary_color'];

        $options['text_on_primary'] = $input['text_on_primary'];

        $options['hide_logo'] = $input['hide_logo'];

        $options['hide_space'] = $input['hide_space'];
    }


    if($settings_tab == 'translation') {
        $options['trans'] = $_POST['boxit_options']['trans'];
    }



    //save only the options that were changed
    $options = array_merge(get_option('boxit_options'), $options);

    //echo '<pre>'; print_r($options); echo '</pre>';

    return $options;
}





/*
 * HELPER
 *
 * Displayes filesize in more user friendly way
 */
function pretty_size($size, $sizes = array('Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')) {
    if ($size == 0) return('n/a');
    return (round($size/pow(1000, ($i = floor(log($size, 1000)))), 2) . ' ' . $sizes[$i]);
}



/*
 * HELPER
 *
 * Converts size like 8M to bytes (80000)
 */
function toBytes($str) {
    $val = trim($str);
    $last = strtolower($str[strlen($str)-1]);
    switch($last) {
        case 'g': $val *= 1024;
        case 'm': $val *= 1024;
        case 'k': $val *= 1024;
    }
    return $val;
}
