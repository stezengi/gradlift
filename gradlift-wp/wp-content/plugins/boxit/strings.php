<?php



/**
 * BOXIT strings array
 * Contains all translatable strings used by BOXIT
 *
 * @var array :
 *  - translation key
 *  - default text
 *  - explanation
 *
 * $boxit_strings servs as a base for translation settings (translation tab)
 */
function get_boxit_strings() {
    $boxit_strings = array(
        /* boxit.js */
        array('syncing_with_dbx', 'syncing with Dropbox', 'Message indicating syncing with dropbox after file upload.'),#85
        array('name_req', 'Name/folder is required', 'Validation error on name/folder field (if enabled)'),#146
        array('email_req', 'Email is required', 'Validation on email field (if enabled)'),#150
        array('wrong_email', 'Email address is invalid', 'Validation on email field (if enabled)'),#157
        /* move_to_dropbox.js */
        array('server_upl_failed', 'Upload to server failed.', 'Upload script error displayed in upload quequ - failed to upload file to server'),#13
        array('mtd_ajax_err', 'BOXIT AJAX ERROR', 'Dropbox script error displayed in upload queue - failed to move file to Dropbox'),#72
        array('all_done', 'All files uploaded', 'Message displayed when all files finish uploading'),#125
        /* index.php */
        array('available_on_d', 'available on Dropbox', 'Dropbox disc space status (ex: 100MB available on Dropbox)'),#163
        array('setup_api_con', 'Set up Dropbox API connection.', 'Setup error displayed on frontend: Dropbox api connection is not set up (BOXIT needs to be installed)'),#163
        array('only_loggedin', 'Only logged in users can use BOXIT', 'Access error displayed if BOXIT is set to be accessible only for logged in users and "vistor" tries to acces it'),#166
        array('all_files', 'all filetypes', 'Information displayed inside the "dropzone" - indicates that all filetypes are allowd for upload'),#181
        array('drop_here', 'Drag & drop to upload', 'Information displayed inside the "dropzone"'),#191
        array('release', 'Release file(s) now', 'Information displayed inside the "dropzone"'),#192
        array('select', 'Select files', 'Select files button label (inside the "dropzone")'),#193
        array('allowed', 'Filetypes allowed:', 'Text before allowed filetypes (inside the "dropzone")'),#194
        array('max', 'Max. filesize:', 'Text before filesize limit (inside the "dropzone")'),#195
        array('queue', 'Upload queue', 'Upload queue header'),#197
        array('start', 'Start Upload', 'Start upload button label'),#200
        array('name', 'Name / Folder:', 'Name/folder input field label'),#200
        array('email', 'Email:', 'Email input field label'),#231
        array('message', 'Upload message:', 'Upload message field label'),#226
        array('proceed', 'Proceed to upload', '"Proceed to upload" button label after name/email/message fields (if they are enabled)'),#246
        array('upl_to_server', 'uploaded to server', 'File status - upload to server succeeded (filelist in email notification)'),#321
        array('upl_to_server_err', 'server upload error', 'File status - upload to server failed (filelist in email notification)'),#321
        array('upl_to_dbx', 'uploaded to Dropbox', 'File status - upload to Dropbox succeeded (filelist in email notification)'),#322
        array('status', 'status:', 'Text preceding file status (filelist in email notification)'),#323
	    array('not_supported', 'BOXIT is not supported in your browser', 'Mesage displayed in browsers not supported by BOXIT (ex IE <= 9)'),
        array('captcha', 'Write text from the image', 'Captcha input label'),
        array('wrong_captcha', 'Please try again', 'Wrong captcha value error message')
    );
    return $boxit_strings;
}



/**
 * Get translations if they are available
 *
 * returns $__trans array (build as "translation_key" => "translation")
 * $__trans array is used in php files and passed to javascript files as object
 * In javascript files use : __trans.translation_key to get the translation
 */
function boxit_strings() {
    $options = get_option('boxit_options');
    if(!isset($options['trans'])) $options['trans'] = array();
    if(empty($options['trans'])) {
        //if translations are not set up use strings from $boxit_strings array
        $s = get_boxit_strings();
        foreach($s as $str) {
            $__trans[(string)$str[0]] = $str[1];
        }
        return $__trans;
    } else {
        //use strings from translation settings
        foreach ($options['trans'] as $str_key => $str) {
            $t = ($str[1] == '')? $str[0] : $str[1];
            $__trans[$str_key] = $t;
        }
        return $__trans;
    }
}
$__trans = boxit_strings();

