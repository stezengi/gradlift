<div class="wrap ignitiondeck">
	<div class="icon32" id=""></div><h2 class="title"><?php _e('Social Settings', 'idsocial'); ?></h2>
	<div class="help">
		<a href="http://forums.ignitiondeck.com" alt="IgnitionDeck Support" title="IgnitionDeck Support" target="_blank"><button class="button button-large"><?php _e('Support', 'idsocial'); ?></button></a>
		<a href="http://docs.ignitiondeck.com" alt="IgnitionDeck Documentation" title="IgnitionDeck Documentation" target="_blank"><button class="button button-large"><?php _e('Documentation', 'idsocial'); ?></button></a>
	</div>
	<div class="id-settings-container">
		<div class="postbox-container" style="width:100%;">
			<div class="metabox-holder">
				<div class="meta-box-sortables" style="min-height:0;">
					<div class="postbox">
						<h3 class="hndle"><span><?php _e('Social Login', 'idsocial'); ?></span></h3>
						<div class="inside">
							<form action="" method="POST" id="idsocial_settings">
									<div class="form-row third left">
										<label for="app_id"><?php _e('Facebook Application ID', 'idsocial'); ?></label>
										<input id="app_id" name="app_id" value="<?php echo (isset($settings['app_id']) ? $settings['app_id'] : ''); ?>" />
										<a href="https://developers.facebook.com/apps/"><?php _e('Create App', 'idsocial'); ?></a>
									</div>
									<div class="form-row">
										<button class="button button-primary" id="submit_social_settings" name="submit_social_settings"><?php _e('Save', 'idsocial'); ?></button>
									</div>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>