<?php
function idsocial_fblogin() {
	$success = 0;
	if (isset($_POST['User'])) {
		$fb_user = $_POST['User'];
		$email = $fb_user['email'];
		if (!empty($email)) {
			$wp_user = get_user_by('email', $email);
			if (!empty($wp_user)) {
				if (is_user_logged_in()) {
					$success = 1;
				}
				else {
					// would be better if we could perform this at beginning of an init hook
					$override = 0;
					if (isset($_POST['Override'])) {
						$override = $_POST['Override'];
					}
					$logged_out = get_transient('idsocial_logout_'.$wp_user->ID);
					if (!$logged_out || $override) {
						$signon = wp_set_auth_cookie($wp_user->ID, true);
					//if (is_user_logged_in ()) {
						$success = 1;
						if ($override) {
							delete_transient('idsocial_logout_'.$wp_user->ID);
						}
					//}
					}
				}
			}
			else {
				// user does not exist
				$user = array(
					'user_login' => $email,
					'user_email' => $email,
					'first_name' => $fb_user['first_name'],
					'last_name' => $fb_user['last_name'],
					'user_nicename' => $fb_user['name'],
					'nickname' => $fb_user['name'],
					'user_pass' => idf_pw_gen()
					);
				$user_id = wp_insert_user($user);
				if ($user_id > 0) {
					$signon = wp_set_auth_cookie($user_id, true);
					//if (is_user_logged_in()) {
						$success = 1;
					//}
				}
			}
		}
	}
	echo $success;
	exit;
}
add_action('wp_ajax_idsocial_fblogin', 'idsocial_fblogin');
add_action('wp_ajax_nopriv_idsocial_fblogin', 'idsocial_fblogin');

function idsocial_logout() {
	$user = wp_get_current_user();
	if (!empty($user)) {
		set_transient('idsocial_logout_'.$user->ID, 1);
	}
}

add_action('clear_auth_cookie', 'idsocial_logout');
?>