<?php

/*
Plugin Name: IgnitionDeck Social
URI: http://IgnitionDeck.com
Description: Social login, sharing, and analytics for the IgnitionDeck platform
Version: 1.0
Author: Virtuous Giant
Author URI: http://VirtuousGiant.com
License: GPL2
*/

define( 'IDSOCIAL_PATH', plugin_dir_path(__FILE__) );

include 'idsocial-admin.php';
include 'idsocial-functions.php';

add_action('wp_enqueue_scripts', 'id_fb_sdk', 9);

function id_fb_sdk() {
	wp_register_script('facebook', plugins_url('js/facebook.js', __FILE__));
	wp_register_script('idsocial-fb', plugins_url('js/idsocial-fb.js', __FILE__));
	wp_enqueue_script('facebook');
	wp_enqueue_script('idsocial-fb');
	$settings = get_option('idsocial_settings');
	wp_localize_script('facebook', 'idsocial_fb_app_id', (isset($settings['app_id']) ? $settings['app_id'] : ''));
	wp_localize_script('facebook', 'idsocial_logged_in', (is_user_logged_in() ? '1' : '0'));
}

add_action('idc_below_login_form', 'idsocial_show_fblogin');
add_action('idc_below_register_form', 'idsocial_show_fblogin');

function idsocial_show_fblogin() {
	echo '<div class="center"><div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="false" onlogin="idsocial_fblogin_callback" scope="email, user_friends, public_profile"></div></div>';
}
?>