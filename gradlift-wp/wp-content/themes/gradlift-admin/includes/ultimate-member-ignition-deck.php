<?php
/**
 * Custom functions for integration between Ultimate Member and IgnitionDeck
 */
 
function gradlift_ok_for_project( $user_id ) {
	//prar($user_id);
	$user_info = get_userdata( $user_id );
	if(!$user_info) return false;
	prar($user_info);
	$user_meta = get_user_meta($user_id);
	prar($user_meta);
}
 
function gradlift_project_exists( $user_id ) {
	
	
	 $args = array(
	'posts_per_page'   => 5,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'ignition_product',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'	   => $user_id,
	'post_status'      => 'publish',
	'suppress_filters' => true 
);
$projects = get_posts( $args ); 
//prar('checking for project');
//prar($projects);
	 wp_reset_postdata();
	if( isset( $projects[0] ) ) {
		prar('project created');
		return true;
	} else {
		prar('project not created');
		return false;
	}
}
//echo '<!--umid functions -->';
add_action( 'user_register', 'gradlift_um_new_user_add_project', 10, 1 );

//add_action('um_after_new_user_register', 'gradlift_um_new_user_add_project', 50, 2 );
	//function um_after_new_user_register($user_id, $args){

function gradlift_um_new_user_add_project( $user_id ) {
	//$user_id = 15;
	//prar("user_id: $user_id<br>");
	if( !gradlift_ok_for_project( $user_id ) ) return null;
	if( gradlift_project_exists( $user_id ) ) return null;
	$user_info = get_user_meta($user_id);
	//prar($user_info);
	//if( $user_info['wp_b78jsz6f89_user_level'][0] != 2 ) return null;
	if( $user_info['role'][0] != 'member' ) return null;
	//exit();
    //if ( isset( $_POST['first_name'] ) )
        //update_user_meta($user_id, 'first_name', $_POST['first_name']);
	//update_user_meta($user_id, 'first_name', 'TEST');
	$project_category = null;
	$project_name = $user_info['first_name'][0] . ' ' . $user_info['last_name'][0];
	//echo $project_name;
	$project_goal = 1500;
	$project_type = 'level-based';
	$project_end_type = 'open';
	$args = array(
				'post_author' => $user_id, //GradLift userID is 1
				'post_title' => $project_name,
				'post_type' => 'ignition_product',
				'tax_input' => array('project_category' => $project_category));
	$args['post_status'] = 'draft';
	$post_id = wp_insert_post($args);
	wp_set_object_terms($post_id, $project_category, 'project_category');
	$saved_levels = array();
	$saved_levels[0]['title'] = 'GradLift';
	$saved_levels[0]['limit'] = null;
	$saved_levels[0]['short'] = 'Student';
	$saved_levels[0]['price'] = 5;
	$project_fund_type = get_option('idc_cf_fund_type');
			if (empty($project_fund_type)) {
				$project_fund_type = 'capture';
			}
	// Insert to ign_products
				$proj_args = array('product_name' => $project_name);
				//if (isset($saved_levels[0])) {
					$proj_args['ign_product_title'] = $saved_levels[0]['title'];
					$proj_args['ign_product_limit'] = $saved_levels[0]['limit'];
					$proj_args['product_details'] = $saved_levels[0]['short'];
					$proj_args['product_price'] = $saved_levels[0]['price'];
				//}
				$proj_args['goal'] = $project_goal;
				$project_id = get_post_meta($post_id, 'ign_project_id', true);
				if (!empty($project_id)) {
					$project = new ID_Project($project_id);
					$project->update_project($proj_args);
				}
				else {
					$project_id = ID_Project::insert_project($proj_args);
				}
	
				if (isset($project_id)) {
					
					
					//update_post_meta($post_id, 'ign_product_name', $project_name);
					//update_post_meta($post_id, 'ign_start_date', $project_start);
					//update_post_meta($post_id, 'ign_fund_end', $project_end);
					update_post_meta($post_id, 'ign_fund_goal', $project_goal);
					//update_post_meta($post_id, 'ign_proposed_ship_date', $project_ship_date);
					//update_post_meta($post_id, 'ign_project_description', $project_short_description);
					//update_post_meta($post_id, 'ign_project_long_description', $project_long_description);
					//update_post_meta($post_id, 'ign_faqs', $project_faq);
					//update_post_meta($post_id, 'ign_updates', $project_updates);
					//update_post_meta($post_id, 'ign_product_video', $project_video);


					update_post_meta($post_id, 'ign_project_id', $project_id);
					update_post_meta($post_id, 'ign_project_type', $project_type);
					update_post_meta($post_id, 'ign_end_type', $project_end_type);
					if (empty($purchase_form)) {
						update_post_meta($post_id, 'ign_option_purchase_url', 'default');
					}
					// levels
					//update_post_meta($post_id, 'ign_disable_levels', $disable_levels);
					$project_levels = 1;
					update_post_meta($post_id, 'ign_product_level_count', $project_levels);
					update_post_meta($post_id, 'ign_product_title', $saved_levels[0]['title']); /* level 1 */
					update_post_meta($post_id, 'ign_product_price', $saved_levels[0]['price']); /* level 1 */
					update_post_meta($post_id, 'ign_product_short_description', $saved_levels[0]['short']); /* level 1 */
					//update_post_meta($post_id, 'ign_product_details', $saved_levels[0]['long']); /* level 1 */
					//update_post_meta($post_id, 'ign_product_limit', $saved_levels[0]['limit']); /* level 1 */

					/*for ($i = 2; $i <= $project_levels; $i++) {
						update_post_meta($post_id, 'ign_product_level_'.($i).'_title', $saved_levels[$i-1]['title']);
						update_post_meta($post_id, 'ign_product_level_'.($i).'_price', $saved_levels[$i-1]['price']);
						update_post_meta($post_id, 'ign_product_level_'.($i).'_short_desc', $saved_levels[$i-1]['short']);
						update_post_meta($post_id, 'ign_product_level_'.($i).'_desc', $saved_levels[$i-1]['long']);
						update_post_meta($post_id, 'ign_product_level_'.($i).'_limit', $saved_levels[$i-1]['limit']);
					}*/
					// Attach product to user
					set_user_projects($post_id, $user_id);
					if (!isset($status)) {
						do_action('ide_fes_create', $user_id, $project_id, $post_id, $proj_args, $saved_levels, $project_fund_type);
					}
					else {
						do_action('ide_fes_update', $user_id, $project_id, $post_id, $proj_args, $saved_levels, $project_fund_type);
					}
					$company_name = null;
					$company_logo = null;
					$company_location = null;
					$company_url = null;
					$company_fb = null;
					$company_twitter = null;
					$project_start = null;
					$project_end = null;
					$project_ship_date = null;
					$project_short_description = null;
					$project_long_description = null;
					$project_faq = null;
					$project_updates = null;
					$project_video = null;
					$project_hero = null;
					$project_image2 = null;
					$project_image3 = null;
					$project_image4 = null;
					$disable_levels = null;
					
					$vars = array('post_id' => $post_id,
						'company_name' => $company_name,
						'company_logo' => $company_logo,
						'company_location' => $company_location,
						'company_url' => $company_url,
						'company_fb' => $company_fb,
						'company_twitter' => $company_twitter,
						'project_name' => $project_name,
						'project_category' => $project_category,
						'project_start' => $project_start,
						'project_end' => $project_end,
						'project_goal' => $project_goal,
						'project_ship_date' => $project_ship_date,
						'project_short_description' => $project_short_description,
						'project_long_description' => $project_long_description,
						'project_faq' => $project_faq,
						'project_updates' => $project_updates,
						'project_video' => $project_video,
						'project_hero' => $project_hero,
						'project_image2' => $project_image2,
						'project_image3' => $project_image3,
						'project_image4' => $project_image4,
						'project_id' => $project_id,
						'project_type' => $project_type,
						'project_fund_type' => $project_fund_type,
						'project_end_type' => $project_end_type,
						'disable_levels' => $disable_levels,
						'project_levels' => $project_levels,
						'levels' => $saved_levels
					);
					do_action('ide_fes_submit', $post_id, $project_id, $vars);
					//echo '<script>location.href="'.apply_filters('ide_fes_submit_redirect', '?edit_project='.$post_id).'";</script>';
				}
				else {
					// return some error
				}
				
			if(isset( $post_id )) {
				wp_publish_post( $post_id );
			}
}
/*
Array
(
    [nickname] => Array
        (
            [0] => mlamarr25@gmail.com
        )

    [first_name] => Array
        (
            [0] => Michael
        )

    [last_name] => Array
        (
            [0] => LaMarr
        )

    [description] => Array
        (
            [0] => 
        )

    [rich_editing] => Array
        (
            [0] => true
        )

    [comment_shortcuts] => Array
        (
            [0] => false
        )

    [admin_color] => Array
        (
            [0] => fresh
        )

    [use_ssl] => Array
        (
            [0] => 0
        )

    [show_admin_bar_front] => Array
        (
            [0] => true
        )

    [wp_b78jsz6f89_capabilities] => Array
        (
            [0] => a:4:{s:20:"create_edit_projects";b:1;s:12:"upload_files";b:1;s:6:"author";b:1;s:15:"bbp_participant";b:1;}
        )

    [wp_b78jsz6f89_user_level] => Array
        (
            [0] => 2
        )

    [role] => Array
        (
            [0] => member
        )

    [submitted] => Array
        (
            [0] => a:10:{s:7:"form_id";s:4:"1099";s:9:"timestamp";s:10:"1424839924";s:7:"request";s:0:"";s:4:"role";s:6:"member";s:10:"first_name";s:7:"Michael";s:9:"last_name";s:6:"LaMarr";s:10:"user_email";s:19:"mlamarr25@gmail.com";s:13:"user_password";s:9:"Sarah1993";s:21:"confirm_user_password";s:9:"Sarah1993";s:10:"user_login";s:19:"mlamarr25@gmail.com";}
        )

    [user_password] => Array
        (
            [0] => Sarah1993
        )

    [full_name] => Array
        (
            [0] => michael.lamarr
        )

    [account_status] => Array
        (
            [0] => approved
        )

    [profile_photo] => Array
        (
            [0] => profile_photo.jpg
        )

    [cover_photo] => Array
        (
            [0] => cover_photo.jpg
        )

    [Employer] => Array
        (
            [0] => Edwards Air Force Base
        )

    [position] => Array
        (
            [0] => Human Factors Engineer
        )

    [school] => Array
        (
            [0] => California State Univerity Northridge
        )

    [degreetype] => Array
        (
            [0] => Master's
        )

    [subjects] => Array
        (
            [0] => Advanced Cognitive Psychology, Human Factors design, Advanced Psychology, Research, Usability, Statistics
        )

    [major] => Array
        (
            [0] => Human Factors and Applied Psychology
        )

    [mystory] => Array
        (
            [0] => As soon as I started my masters program, I knew that I needed to fully immerse myself in school. I quit my job at Costco and started an internship at Systems Engineering Research Laboratory (SERL) where I got to conduct research projects with NASA. I also got to dabble in design at Tseng College of Distance Learning and moderated usability research at Activision and even got my name in the credits of Band Hero.
        )

    [session_tokens] => Array
        (
            [0] => a:6:{s:64:"60ae248f20b5d1552747db5becd3785e52b590ea7f77bb04be65d57b897315cb";a:4:{s:10:"expiration";i:1438147294;s:2:"ip";s:13:"104.35.148.64";s:2:"ua";s:68:"Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";s:5:"login";i:1436937694;}s:64:"b16c2f9431a8a16c1f295adb7d40f3fc479de4246048a09708999542b9b013db";a:4:{s:10:"expiration";i:1438658409;s:2:"ip";s:13:"67.53.250.130";s:2:"ua";s:68:"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";s:5:"login";i:1437448809;}s:64:"c0f6f9e616cb7a557b8a4847d11d7a06b3cd5e4260a9110acd4734e45e2ff0b7";a:4:{s:10:"expiration";i:1438658768;s:2:"ip";s:13:"67.53.250.130";s:2:"ua";s:134:"Mozilla/5.0 (iPhone; CPU iPhone OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H143 Safari/600.1.4";s:5:"login";i:1437449168;}s:64:"b6c4a0f26bf469358130d2910c8e25c1e5bb9f33e284f3187d039b89af6538ef";a:4:{s:10:"expiration";i:1438746222;s:2:"ip";s:13:"67.53.250.130";s:2:"ua";s:68:"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";s:5:"login";i:1437536622;}s:64:"2675a7138fb1a799929553fd1799e07ff63006db7b0db001a6a318f75bd601ba";a:4:{s:10:"expiration";i:1439098544;s:2:"ip";s:13:"67.53.250.130";s:2:"ua";s:108:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36";s:5:"login";i:1437888944;}s:64:"10ee020e825f6c9a7a7aeff44ec9141d3dcc7d6d65b5e7f09b61ab54814c8135";a:4:{s:10:"expiration";i:1439099126;s:2:"ip";s:13:"67.53.250.130";s:2:"ua";s:68:"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko";s:5:"login";i:1437889526;}}
        )

    [facebook] => Array
        (
            [0] => https://www.facebook.com/michael.lamarr
        )

    [myprojects] => Array
        (
            [0] => 
        )

    [schoolfromseason] => Array
        (
            [0] => Fall
        )

    [schoolfromyear] => Array
        (
            [0] => 2007
        )

    [schoolfromseason_27] => Array
        (
            [0] => Fall
        )

    [schoolfromyear_28] => Array
        (
            [0] => 2010
        )

    [ide_user_projects] => Array
        (
            [0] => s:17:"a:1:{i:0;i:1180;}";
        )

    [gradstory] => Array
        (
            [0] => Learning has always been my passion, but unfortunately it landed me in some serious student debt! I was lucky enough to have parents who helped me out while I earned my Bachelorâ€™s degree, but as soon as I started my Masterâ€™s program I had to take responsibility for my tuition. While earning my Masterâ€™s, I had to take time off from work to develop my thesis and pursue amazing internship opportunities with NASA and Activision. Even though my experience was awesome, my student debt was all the way up to the $50,000 mark so any donation to help me pay back my current $8,000 dollar student debt will help out greatly.
        )

    [Employer_12] => Array
        (
            [0] => Edwards Air Force Base
        )

    [position_13] => Array
        (
            [0] => Human Factors Engineer
        )

    [school_30] => Array
        (
            [0] => California State University Northridge
        )

    [schoolfromseason_31] => Array
        (
            [0] => Fall
        )

    [schoolfromseason_27_32] => Array
        (
            [0] => Spring
        )

    [schoolfromyear_33] => Array
        (
            [0] => 2007
        )

    [schoolfromyear_28_34] => Array
        (
            [0] => 2005
        )

    [degreetype_35] => Array
        (
            [0] => Bachelor's
        )

    [major_36] => Array
        (
            [0] => Psychology
        )

    [subjects_38] => Array
        (
            [0] => Cognitive psychology, Child Psychology, Abnormal Psychology, Ethics, College algebra, Research, Human factors, Statistics
        )

    [mystory_39] => Array
        (
            [0] => Once I started my junior year of college I was very focused on succeeding, but I still didnâ€™t know what I wanted to do when I completed my program. I was on the deanâ€™s list every semester until I graduated and I took a wide variety of psychology classes until I figured out what I wanted. Human factors was my golden ticket and I have been very focused on pursuing my education in this field every since.
        )

    [myprojects1] => Array
        (
            [0] => â€¢Conducted a human-in-the-loop study to determine how descriptive waypoints may improve flight performance during Continuous Descent Approach procedures. This work was done in collaboration with NASA human factors researchers.

â€¢Conducted an error analysis of video game testers. The results were used to improve the user interface to remove frustration and to maximize enjoyment.

â€¢Conducted a heuristic analysis on a clothing companyâ€™s website. The feedback was used in redesigning the website to reduce clutter, increase readability, and improve the user experience.

â€¢Designed an aircraft primary flight display that used synthetic vision and displays traffic information to the pilot.

â€¢Designed a transportation system for a space station; a project that consisted of 12 subsystems and required collaboration for success.

â€¢Conducted usability study comparing two similar websites to observe how users perform searches in different scenarios and made recommendations for improvement.
        )

    [mysubjects] => Array
        (
            [0] => Advanced Cognitive Psychology, Human Factors design, Advanced Psychology, Research, Usability, Statistics
        )

    [mysubjects_2] => Array
        (
            [0] => Cognitive psychology, Child Psychology, Abnormal Psychology, Ethics, College algebra, Research, Human factors, Statistics
        )

    [college_2] => Array
        (
            [0] => Yes
        )

    [didyouattendanothercollege_2] => Array
        (
            [0] => Yes
        )

    [schoolfromseason_31_42] => Array
        (
            [0] => Fall
        )

    [schoolfromseason_27_32_43] => Array
        (
            [0] => Spring
        )

    [schoolfromyear_28_34_44] => Array
        (
            [0] => 2002
        )

    [schoolfromyear_33_45] => Array
        (
            [0] => 2005
        )

    [school_30_47] => Array
        (
            [0] => College of the Canyons
        )

    [degreetype_35_48] => Array
        (
            [0] => Associates
        )

    [major_36_49] => Array
        (
            [0] => Transfer Studies
        )

    [mysubjects_2_50] => Array
        (
            [0] => Film, Real Estate, General Psychology, Neuropsychology, Biology, Statistics
        )

    [mystory_39_52] => Array
        (
            [0] => Like many other college students, I did not know what I wanted to major in. With the lack of a major came a lack of dedication and passion. I tried majoring in film, and then real estate for over a year with little success in getting excited about the major. Once I discovered psychology, my grades improved along with my hunger for knowledge. I started getting Aâ€™s in classes and transferred over to California State University Northridge.
        )

    [current_school_dates] => Array
        (
            [0] => Fall 2007 - Fall 2010
        )

    [current_school_dates_50] => Array
        (
            [0] => Fall 2005 - Spring 2007
        )

    [current_school_dates_50_47] => Array
        (
            [0] => Fall 2002 - Spring 2005
        )

    [are_transcripts_submitted] => Array
        (
            [0] => false
        )

    [is_summary_submitted] => Array
        (
            [0] => true
        )

    [are_loans_submitted] => Array
        (
            [0] => true
        )

    [paid_app_fee] => Array
        (
            [0] => true
        )

    [agreed_terms] => Array
        (
            [0] => true
        )

    [stripe_customer_id] => Array
        (
            [0] => cus_6ELOwwKRc9rjMV
        )

    [high_school] => Array
        (
            [0] => No
        )

    [school_46] => Array
        (
            [0] => Valencia High School
        )

    [current_school_dates_47] => Array
        (
            [0] => Graduated 2002
        )

    [myprojects1_48] => Array
        (
            [0] => did stuff
        )

    [_yoast_wpseo_profile_updated] => Array
        (
            [0] => 1437888973
        )

    [_reviews_avg] => Array
        (
            [0] => 0
        )

    [_reviews_total] => Array
        (
            [0] => 0
        )

)
*/

?>