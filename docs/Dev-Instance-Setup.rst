1) Download and install vagrant
2) Download and install virtual box
3) cd to the projects root directory, where you can locate Vagrantfile file
4) Run the following command.
    vagrant up --provider=virtualbox
5) vagrant ssh
6) cd /vagrant/gradlift/src
./manage.py runserver 0.0.0.0:8009

