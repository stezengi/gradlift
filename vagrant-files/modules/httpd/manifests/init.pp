# 

class httpd {
    
    
    package { 'httpd':
      ensure => installed,
    }

    package { 'curl':
      ensure => "present",
    }

    service { "httpd":
		ensure		=> running,
		enable 		=> true,
		name		=> "httpd",
		hasrestart 	=> true,
		hasstatus 	=> true,
    }

    file { 'gradlift-wp-vhost':
		ensure		=> 'present',
		path		=> '/etc/httpd/conf.d/gradlift-wp-vhost.conf',
		source		=> 'puppet:///modules/httpd/gradlift-wp-vhost.conf',
		owner		=> 'vagrant',
                notify          => Service["httpd"],
    }

    file { 'hosts':
		ensure		=> 'present',
		path		=> '/etc/hosts',
		source		=> 'puppet:///modules/httpd/hosts',
		owner		=> 'vagrant',
                notify          => Service["httpd"],
    }

    Package['httpd'] ->
    Package['curl'] ->
    File['gradlift-wp-vhost'] ->    
    File['hosts']
} 

