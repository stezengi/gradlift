
# Installs the base packages needed for development.  All
# platforms will require these basic things in order to checkout
# a project, build and run it.
#
class develop ($unix_user = "vagrant") {
    require mysqld
    Class["django"] -> Class["develop"]

    # Open network access when developing
    service { "iptables":
        ensure    => stopped,
        enable    => false,
    }

    # Install wget (required to install VBox additions, if doing so manually)
    package { "wget":
      ensure => present,
    }

    # Always use Python2.7 environment when developing
    exec { "python27-enable":
        require => Package["python27-python"],
        unless => "grep 'scl enable python27' ~$unix_user/.bash_profile 2>/dev/null",
        command => "echo 'exec scl enable python27 bash' >> ~$unix_user/.bash_profile",
        user  => "vagrant",
        path => ["/bin", "/usr/bin", "/usr/local/bin"],
    }

    file { '/var/log/celery.log':
        ensure => 'file',
        owner => 'vagrant',
        group => 'vagrant',
    }

    file { '/var/log/celery_tasks.log':
        ensure => 'file',
        owner => 'vagrant',
        group => 'vagrant',
    }

  # Always use Python2.7 environment when developing
    exec { "timezone-adjustment":
        command => "sudo mv /etc/localtime /etc/localtime.bak; sudo ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime;",
        user  => "vagrant",
        path => ["/bin", "/usr/bin", "/usr/local/bin"],
        creates => '/etc/timezone-adjusted',
    }

}