
class projectsetup {

  exec { 'gradlift-database-user':
		command		=> 'chmod +x /usr/share/puppet/modules/projectsetup/files/gradlift-database-user.sh; /usr/share/puppet/modules/projectsetup/files/gradlift-database-user.sh',
		path		=> [
			'/usr/bin',
			'/bin',
			'/sbin',
			'/usr/sbin',
			'/usr/local/bin',
		],
		creates		=> '/root/db.gradlift.user',
		logoutput	=> true,
		user		=> 'root',
	}

    # run any deltas which haven't been run on the database yet
	exec { 'run-deltas':
		command		=> 'chmod +x /usr/share/puppet/modules/projectsetup/files/run_deltas.sh; /usr/share/puppet/modules/projectsetup/files/run_deltas.sh',
		path		=> [
			'/usr/bin',
			'/bin',
			'/sbin',
			'/usr/sbin',
			'/usr/local/bin',
		],
		# creates		=> '/root/db.deltas',
		logoutput	=> true,
		user		=> 'root',
	}
	

	EXEC['gradlift-database-user'] ->
        EXEC['run-deltas']

} 

