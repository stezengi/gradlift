
class mysqld {

    package { 'mysql-server':
        ensure => installed,
    }

    service { "mysqld":
        ensure		=> running,
        enable 		=> true,
        name		=> "mysqld",
        hasrestart 	=> true,
        hasstatus 	=> true,
    }

    Package['mysql-server'] ->
    Service['mysqld']
} 

