class subversion {

        package { 'subversion':
                ensure		=> 'present',
                name		=> 'subversion',
        }

        file { 'svn-folder':
                ensure	=> directory,
                path	=> '/root/.subversion',
                owner	=> 'vagrant',
                group	=> 'vagrant',
        }

        file { 'svn-settings':
                ensure	=> present,
                path	=> '/root/.subversion/servers',
                source	=> '/vagrant/vagrant-files/servers',
                owner	=> 'vagrant',
                group	=> 'vagrant',
        }

        Package['subversion'] ->
        File['svn-folder'] ->
        File['svn-settings']

}
