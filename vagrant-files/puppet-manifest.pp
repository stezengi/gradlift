# The main puppet manifest. This controls all the flow, has some smaller classes
# and includes our other modules.

class { 'redis::install':
    redis_version => '2.8.22',
}

redis::server { 'redis-instance':
    redis_ip        => '0.0.0.0',
    redis_port      => 6379,
    redis_memory    => '1g',
    redis_nr_dbs    => 2,
    running         => true,
    enabled         => true
}


class mail {
	package { 'mail':
		ensure 	=> 'present',
		name => 'mailx'
	}
}


class rsync {
	package { 'rsync':
		ensure 	=> 'present',
		name	=> 'rsync',
	}
}
 

class daymessage {

        # message of the day. users will see this when logging in
        $motdStr = "\n Welcome to the GradLift Development Environment. This is a Linux VM running ${operatingsystem} ${operatingsystemrelease} on ${virtual}. Configured by Puppet ${puppetversion}.\n\n\t* Mysql credentials: gradliftwp/123456\n\t* System credentials: vagrant/vagrant & root/vagrant\n\t* IP address: 192.168.12.12\n\t* Hosts lines to add:\n\t\t192.168.12.12   api.gradlift.local\n\t*  \n\t please follow it up with a vagrant reload and vagrant provision\n\n"

        file { 'motd':
                ensure  => file,
                path    => '/etc/motd',
                mode    => 0644,
                content	=> $motdStr,
        }

}

class ftp{

        package { 'ftp':
                ensure		=> 'present',
                name		=> 'ftp',
        }

}

class c {

        package { 'ctags':
                ensure		=> 'present',
                name		=> 'ctags',
        }

}

class restartservices {

    exec { "apache-service-restart":
      command => '/sbin/service httpd restart; exit 0',
    }

    exec { "mysqld-service-restart":
        command => '/sbin/service mysqld restart; exit 0';
    } 
}

class git {
    package { 'git':
                ensure		=> 'present',
                name		=> 'git',
    }
}

class synclocalserverfiles {

    $updirs = ['/var/www/' ,
               '/var/www/gradlift/',
               '/var/www/httpd/' ,
               '/var/www/httpd/Server/',
               '/var/www/httpd/Server/logs/',
               '/var/log/django',
               '/var/run/redis']

    file { $updirs :
                ensure          => 'directory',
                mode            => 755,
    }

    file { ['/var/www/gradlift/private-file-uploads/','/var/www/gradlift/media/'] :
                ensure          => 'directory',
                mode            => 777,
    }

    file { 'up-symlink':
                ensure          => link,
                path            => '/var/www/httpd/up/',
                target          => '/vagrant/',
                owner           => 'vagrant' ,
                group       => "vagrant" ,
                mode        => 0755 ,
    }

    File[$updirs] ->
    File['up-symlink']

}

# include all the classes/modules we want to use

include synclocalserverfiles
include mail 
include rsync
include ftp
include c
include git
include subversion
include httpd
include mysqld
include php
include python
include django
include develop
include projectsetup
include restartservices
include daymessage




# Define the Order
Class['synclocalserverfiles'] ->
Class['mail'] ->
Class['rsync'] ->
Class['ftp'] ->
Class['c'] ->
Class['git'] ->
Class['subversion']  ->
Class['httpd']    ->
Class['mysqld']    ->
Class['php'] ->
Class['python'] ->
Class['django'] ->
Class['develop'] ->
Class['projectsetup'] ->
Class['restartservices'] ->
Class['daymessage'] ->
Class['redis::install']

# end