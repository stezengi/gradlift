
========================
Setup the development environment
========================

/vagrant/gradlift/deploy/update.sh


========================
Run the development environment
========================
cd /vagrant/gradlift/src
./manage.py runserver 0.0.0.0:8009


========================
add dev.gradlift.com with the the ip of VM to your hosts file
========================