1) Read the following document and follow it to setup apps
http://artandlogic.com/2014/04/tutorial-adding-facebooktwittergoogle-authentication-to-a-django-application/
add necessary redirects in gradlift's facebook app by going to https://developers.facebook.com/ , clicking on settings
in the left menu and then choosing advanced and in the client's auth settings section enter the following urls
<protocol>://<domain>/complete/facebook/
for instance
https://qa.gradlift.com/complete/facebook/

2) Add Necessary APIs
You need to add the Google+ API to the list of enabled APIs on the Google Developer Console (under APIs)
https://cloud.google.com/console

3) Configure Google+ redirects
Go to API Manager in https://cloud.google.com/console and click on credentials in the right side bar, click on the app
and then add the redirect urls which should be as described bellow
<protocol>://<domain>/complete/google-oauth2/
for instance
https://qa.gradlift.com/complete/google-oauth2/

4) Add Linked in app and configure its redirects
https://www.linkedin.com/developer/apps/
go to https://www.linkedin.com/developer/apps and click on gradlift's app the add the redirect urls as described below
<protocol>://<domain>/complete/linkedin-oauth2/
for instance
http://qa.gradlift.com/complete/linkedin-oauth2/