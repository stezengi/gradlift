#!/usr/bin/env bash

PROJECT_DIR=`dirname $0`/..
VENV=`dirname $0`/../../../virtualenv/

if [ -d "/vagrant/" ]; then
  VENV=~/virtualenv
fi

# Confirm we're definitely running inside the VM or on a server, otherwise we'll bail.
HOSTNAME=`hostname`
grep 'CentOS' /etc/redhat-release 2>&1 >/dev/null
ON_CENTOS=$?

# Create a virtualenv if it doesn't exist.
#VENV="${PROJECT_DIR}/$PROJECT_ENV"
PYTHON="${VENV}/bin/python"
if [ ! -f "${PYTHON}" ]; then
    echo "creating python virtualenv ${VENV}"
    virtualenv-2.7 $VENV
fi

echo "updating python dependencies."

${VENV}/bin/pip install -r "${PROJECT_DIR}/requirements.txt"
if [ "x$?" != "x0" ]; then
    echo "unable to install python dependencies.  check your network and try again."
    exit 1
fi

# Update the database schema.
MANAGE=${PROJECT_DIR}/src/manage.py

$PYTHON $MANAGE migrate --noinput
$PYTHON $MANAGE collectstatic --noinput

echo "Done."
