sudo systemctl stop uwsgi
sudo systemctl stop nginx
sudo systemctl stop redis.service
sudo /etc/init.d/celeryd stop
sudo /etc/init.d/celerybeat stop

sudo systemctl start uwsgi
sudo systemctl start nginx
sudo systemctl start redis.service
sudo /etc/init.d/celeryd start
sudo /etc/init.d/celerybeat start

sudo systemctl enable nginx
sudo systemctl enable uwsgi
sudo systemctl enable redis.service

sudo redis-cli flushall

sudo kill $(sudo lsof -t -i:80)
sudo /usr/sbin/nginx

echo "Disregard the message above"
echo "nginx and uwsgi restarted successfully"
