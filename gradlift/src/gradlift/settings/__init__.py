
from default import *

# If it is not production (production module does not exist), then try to get qa settings.
try:
    from production import *
except ImportError as e:
    pass

try:
    from qa import *
except ImportError as e:
    pass

from celery import schedules


# :::::::::::::::::::::::::::::::::::::::
#     CELERYBEAT CONFIG
# :::::::::::::::::::::::::::::::::::::::
CELERYBEAT_SCHEDULE = {
    'send-notification-emails-daily': {
        'task': 'gradlift.apps.data.tasks.send_notification_emails_daily',
        'schedule': schedules.crontab(
            minute=CELERY_SEND_NOTIFICATION_EMAILS_DAILY['minute'],
            hour=CELERY_SEND_NOTIFICATION_EMAILS_DAILY['hour'],
        ),
        'args': (),
    },
    'send-notification-emails-weekly': {
        'task': 'gradlift.apps.data.tasks.send_notification_emails_weekly',
        'schedule': schedules.crontab(
            minute=CELERY_SEND_NOTIFICATION_EMAILS_WEEKLY['minute'],
            hour=CELERY_SEND_NOTIFICATION_EMAILS_WEEKLY['hour'],
            day_of_week=CELERY_SEND_NOTIFICATION_EMAILS_WEEKLY['day_of_week'],
        ),
        'args': (),
    },
    'get-database-backups-daily': {
        'task': 'gradlift.apps.data.tasks.get_database_backups_daily',
        'schedule': schedules.crontab(
            minute=CELERY_GET_DATABASE_BACKUPS_DAILY['minute'],
            hour=CELERY_GET_DATABASE_BACKUPS_DAILY['hour'],
        ),
        'args': (),
    },
}