from __future__ import print_function
from django.conf import settings
from subprocess import call

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = """ Drops the tables: manage.py drop_tables """

    def handle(self, *args, **options):

        db_params = settings.DATABASES['default']
        drop_command = 'mysql -u{user} -p{pw} -e \'drop database `{db}`\''.format(user=db_params['USER'],
                                                                                  pw=db_params['PASSWORD'],
                                                                                  db=db_params['NAME']),
        call(drop_command, shell=True)

        create_command = 'mysql -u{user} -p{pw} -e \'create database `{db}` character set utf8 collate ' \
                         'utf8_general_ci\''.format(user=db_params['USER'], pw=db_params['PASSWORD'], db=db_params['NAME'])

        call(create_command, shell=True)
