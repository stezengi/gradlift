from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db.models import Q
import datetime

from gradlift.apps.data import models


def get_today():
    return datetime.datetime.now().date()


def validate_referral_code(value):

    try:
        referral_code = models.ReferralCode.objects.get(code=value)
    except models.ReferralCode.DoesNotExist:
        raise ValidationError('This referral code does not exist.')

    if not referral_code.is_active:
        raise ValidationError('This referral code is no longer active.')

    expiration_date = referral_code.expiration_date
    if expiration_date and (referral_code.expiration_date < get_today()):
        raise ValidationError('This referral code has expired on {expiration}.'.format(
            expiration=expiration_date.strftime("%m/%d/%Y")))


def validate_email(value):

    if User.objects.filter(Q(email=value) | Q(username=value)).exists():
        raise ValidationError('A user with this email already exists. If you forgot your password, follow the ' +
                              '"forgot password" process in login page.')
