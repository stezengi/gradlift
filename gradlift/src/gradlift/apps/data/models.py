import logging
import os
import datetime
import stripe

from django.contrib.staticfiles.templatetags.staticfiles import static

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse

from django.db import models
from django.db import IntegrityError
from django.db.models import Q, Sum, F

from itertools import islice

from pytz import timezone

from gradlift.apps import Constants
from gradlift.apps.data import tasks

logger = logging.getLogger(__name__)


AuthenticationForm.base_fields['username'].max_length = 75


class RefModelDropDownOptions(object):
    def get_options(self):
        """
        Returns the list of objects formatted for drop downs; (('code', 'name'), ('code', 'name))

        """
        options = list(self.values_list('code', 'name'))
        options.insert(0, (u'', u'Select One'))
        return options


class RefUserType(models.Model):
    """
    User types model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_ref_user_type'

    def __str__(self):
        return self.name


class RefCountryManager(models.Manager, RefModelDropDownOptions):
    """
    Manager for countries model

    """
    def get_usa(self):
        """
        Returns USA from the list of countries

        """
        return self.get(code="US")


class RefCountry(models.Model):
    """
    Countries model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)

    objects = RefCountryManager()

    class Meta:
        db_table = 'data_ref_country'

    def __str__(self):
        return self.name


class RefStateManager(models.Manager, RefModelDropDownOptions):
    """
    Manager for states model

    """


class RefState(models.Model):
    """
    States model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)

    objects = RefStateManager()

    class Meta:
        db_table = 'data_ref_state'

    def __str__(self):
        return self.name


class RefSeason(models.Model):
    """
    Seasons model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    order = models.SmallIntegerField(default=0)

    class Meta:
        db_table = 'data_ref_season'
        ordering = ['order']

    def __str__(self):
        return self.name


class RefDegreeManager(models.Manager, RefModelDropDownOptions):
    """
    Manager for degrees model

    """


class RefDegree(models.Model):
    """
    Degrees model

    """
    GRAD_DEGREE_CODES = ['MASTER', 'DOCTORAL', 'PRFESSIONAL']

    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    order = models.SmallIntegerField(default=0)

    objects = RefDegreeManager()

    class Meta:
        db_table = 'data_ref_degree'
        ordering = ['order']

    def __str__(self):
        return self.name


class RefMajor(models.Model):
    """
    Majors model

    """
    code = models.CharField(max_length=254)
    name = models.CharField(max_length=254)

    class Meta:
        db_table = 'data_ref_major'

    def __str__(self):
        return self.name


class RefTier(models.Model):
    """
    Tiers model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)
    integer_value = models.IntegerField(default=0)

    class Meta:
        db_table = 'data_ref_tier'
        ordering = ['integer_value']

    def __str__(self):
        return self.name


class RefOrganizationType(models.Model):
    """
    Organization Types model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_ref_organization_type'

    def __str__(self):
        return self.name


class RefContributionTypeManager(models.Manager):
    """
    Manager for contribution types model

    """
    def get_types_dict(self):
        output = []
        for contribution_type in self.all():
            output.append(contribution_type.get_dict())

        return output

    def get_reference_data(self):
        return {
            'payment_options': Constants.PAYMENT_OPTIONS,
            'contribution_types': self.get_types_dict()
        }


class RefContributionType(models.Model):
    """
    Contribution types model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)
    order = models.SmallIntegerField(default=0)

    objects = RefContributionTypeManager()

    class Meta:
        db_table = 'data_ref_contribution_type'
        ordering = ['order']

    def __str__(self):
        return self.name

    def get_dict(self):
        return {
            'code': self.code,
            'name': self.name
        }


class RefSocialNetworkType(models.Model):
    """
    Social Network types model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    order = models.SmallIntegerField(default=0)

    class Meta:
        db_table = 'data_ref_social_network_type'
        ordering = ['order']

    def __str__(self):
        return self.name


class RefNotificationType(models.Model):
    """
    Notifications types model

    """
    TYPE_FUND_PAYMENT_TIME = 'FUND_PAYMENT_TIME'
    TYPE_LOAN_DOCUMENTS_UPLOADED = 'LOAN_DOCUMENTS_UPLOADED'

    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_ref_notification_type'

    def __str__(self):
        return self.name


class OrganizationAgreement(models.Model):
    """
    Organization agreements model

    """
    description = models.TextField(max_length=50000, null=True, blank=True, default=None)
    creation_date = models.DateField(null=True, blank=True, default=None)
    expiration_date = models.DateField(null=True, blank=True, default=None)
    is_active = models.BooleanField(default=False)
    tier = models.ForeignKey(RefTier, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_organization_agreement'

    def __str__(self):
        return "tier: {tier}, expiration date: {expiration_date}, {is_active}".format(
            tier=self.tier,
            expiration_date=self.expiration_date,
            is_active="Active" if self.is_active else "Inactive"
        )


class Organization(models.Model):
    """
    Organizations model

    """
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=254)
    description = models.TextField(max_length=500, null=True, blank=True, default=None)
    agreement = models.OneToOneField(OrganizationAgreement, null=True, blank=True, default=None)
    type = models.ForeignKey(RefOrganizationType, null=True, blank=True, default=None)
    phone_number = models.CharField(max_length=45)
    zip_code = models.CharField(max_length=45, null=True, blank=True, default=None)
    city = models.CharField(max_length=254, null=True, blank=True, default=None)
    street = models.CharField(max_length=500, null=True, blank=True, default=None)
    state = models.ForeignKey(RefState, null=True, blank=True, default=None)
    country = models.ForeignKey(RefCountry, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_organization'

    def __str__(self):
        return "{name}, type: {type}, phone_number: {phone_number}".format(
            name=self.name,
            type=self.type,
            phone_number=self.phone_number
        )


class ReferralCode(models.Model):
    """
    Referral codes model

    """
    code = models.CharField(max_length=254)
    description = models.TextField(max_length=5000, null=True, blank=True, default=None)
    creation_date = models.DateField(null=True, blank=True, default=None)
    expiration_date = models.DateField(null=True, blank=True, default=None)
    is_active = models.BooleanField(default=False)
    organization = models.ForeignKey(Organization, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_referral_code'

    def __str__(self):
        return self.code


class UserProfileManager(models.Manager):
    """
    Manager for user profiles model

    """
    def get_profile_overviews_by_query(self, queryset, number_of_records=0):
        """
        Goes through the given queryset and formats all profile overviews and returns them in an array along with the
        max number of cards per page

        :param queryset:
        :param number_of_records:
        :return: formatted_profile_overviews, max_page_number

        """
        profile_previews_arr = []
        for query in queryset:
            profile_previews_arr.append(query.get_profile_overview_dict())

        max_page_number = int(number_of_records/Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        if (number_of_records % Constants.MAX_NUMBER_OF_CARDS_PER_PAGE) > 0:
            max_page_number += 1

        return profile_previews_arr, max_page_number

    def sort_profile_overviews_query(self, query):
        """
        Orders profile overviews by whether or not they have profile picture and the date they have joined

        :param query: queryset
        :return: queryset

        """
        query = query.extra(select={
            'has_profile_picture': 'LENGTH(profile_picture) > 0'
        })

        return query.order_by((0 * F('student__display_order')).desc(), '-has_profile_picture', '-user__date_joined')

    def discover_profile_overviews(self, search_term, page_number=1):
        """
        Finds profile overviews based on the given search term for the given page number

        :param search_term:
        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        search_terms = search_term.split()

        conditions = (Q(student__isnull=False) &
                      (Q(user__first_name__icontains=search_term) |
                      Q(user__last_name__icontains=search_term) |
                      Q(user__first_name__istartswith=search_term) |
                      Q(user__email__icontains=search_term) |
                      Q(student__colleges__name__icontains=search_term) |
                      Q(student__colleges__major_name__icontains=search_term) |
                      Q(student__colleges__degree__name__icontains=search_term) |
                      Q(student__high_schools__name__icontains=search_term) |
                      Q(street__icontains=search_term) |
                      Q(state__name__icontains=search_term) |
                      Q(country__name__icontains=search_term) |
                      Q(zip_code__icontains=search_term)))

        if len(search_terms) > 1:
            conditions |= (Q(user__first_name__icontains=search_terms[0]) & Q(user__last_name__icontains=search_terms[1]))

        filter_query = self.filter(conditions).distinct()

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        found_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(found_profiles, number_of_records)

    def get_all_profile_overviews(self, page_number=1):
        """
        Returns all profile overview for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(
            user__is_active=True,
            student__isnull=False
        )

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        all_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(all_profiles, number_of_records)

    def get_featured_profile_overviews(self, page_number=1):
        """
        Returns featured profiles for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(user__is_active=True, student__is_featured=True)

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        featured_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(featured_profiles, number_of_records)

    def get_recently_added_profile_overviews(self, page_number=1):
        """
        Returns recently added profiles for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(
            user__is_active=True,
            student__isnull=False
        )

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        recently_added_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(recently_added_profiles, number_of_records)

    def get_high_school_profile_overviews(self, page_number=1):
        """
        Returns high school profiles for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(
            user__is_active=True).exclude(
                Q(student__high_schools__isnull=True) | Q(student__high_schools__name__isnull=True) | Q(student__high_schools__name__exact='')
            ).filter(
                Q(student__colleges__isnull=True) | Q(student__colleges__name__exact='')
            ).distinct()

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        high_school_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(high_school_profiles, number_of_records)

    def get_college_profile_overviews(self, page_number=1):
        """
        Returns college profiles for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(
            user__is_active=True).filter(
                Q(student__colleges__isnull=False),
                Q(student__colleges__name__isnull=False)
            ).exclude(
                Q(student__colleges__name__exact='') |
                Q(student__colleges__degree__code__in=RefDegree.GRAD_DEGREE_CODES)
            ).distinct()

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        college_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(college_profiles, number_of_records)

    def get_grad_school_profile_overviews(self, page_number=1):
        """
        Returns grad school profiles for the given page number

        :param page_number:
        :return: array of formatted profile cards

        """
        start = (page_number-1) * Constants.MAX_NUMBER_OF_CARDS_PER_PAGE
        end = (start + Constants.MAX_NUMBER_OF_CARDS_PER_PAGE)

        filter_query = self.filter(
            user__is_active=True).filter(
                Q(student__colleges__isnull=False),
                Q(student__colleges__name__isnull=False),
                Q(student__colleges__degree__code__in=RefDegree.GRAD_DEGREE_CODES)
            ).exclude(
                Q(student__colleges__name__exact='')
            ).distinct()

        filter_query = self.sort_profile_overviews_query(query=filter_query)

        grad_school_profiles = filter_query[start:end]

        if page_number == 1:
            number_of_records = filter_query.count()
        else:
            number_of_records = 0

        return self.get_profile_overviews_by_query(grad_school_profiles, number_of_records)


class UserProfile(models.Model):
    """
    User profiles model

    """
    NOTIFICATION_CHOICE_INSTANTANEOUSLY = 'instantaneously'
    NOTIFICATION_CHOICE_DAILY = 'daily'
    NOTIFICATION_CHOICE_WEEKLY = 'weekly'

    NOTIFICATION_CHOICES = ((NOTIFICATION_CHOICE_INSTANTANEOUSLY, 'Instantaneously'),
                           (NOTIFICATION_CHOICE_DAILY, 'Daily'),
                           (NOTIFICATION_CHOICE_WEEKLY, 'Weekly'))

    user = models.OneToOneField(User, null=True, blank=True, default=None, related_name='profile')
    referral_code = models.ForeignKey(ReferralCode, null=True, blank=True, default=None)
    cover_image = models.ImageField(upload_to='cover-images/%Y/%m/', null=True, blank=True)
    profile_picture = models.ImageField(upload_to='profile-pictures/%Y/%m/', null=True, blank=True)
    user_type = models.ForeignKey(RefUserType, null=True, blank=True, default=None, related_name='profiles')
    has_agreed_to_terms = models.BooleanField(default=False)
    birthday = models.DateField(null=True, blank=True, default=None)
    zip_code = models.CharField(max_length=45, null=True, blank=True, default=None)
    city = models.CharField(max_length=254, null=True, blank=True, default=None)
    street = models.CharField(max_length=500, null=True, blank=True, default=None)
    state = models.ForeignKey(RefState, null=True, blank=True, default=None)
    country = models.ForeignKey(RefCountry, null=True, blank=True, default=None)
    link = models.CharField(max_length=500)
    notification_frequency = models.CharField(choices=NOTIFICATION_CHOICES,
                                              default=NOTIFICATION_CHOICE_INSTANTANEOUSLY,
                                              max_length=100)

    objects = UserProfileManager()

    class Meta:
        db_table = 'data_user_profile'

    def __str__(self):
        return "{name}, email: {email}".format(
            name=self.user.first_name + " " + self.user.last_name,
            email=self.user.email
        )

    def save(self, *args, **kwargs):
        """
        Saves the profile and if it doesn't have a specific link, it creates one for it.

        :param args:
        :param kwargs:
        :return: None

        """
        super(UserProfile, self).save(*args, **kwargs)
        if not self.link:
            self.setup_link()

    @property
    def is_profile_completed(self):
        """
        Determines whether or not the current profile is complete

        :return: Boolean

        """
        for college in self.student.colleges.all():
            if len(college.name) > 0:
                return True

        for high_school in self.student.high_schools.all():
            if len(high_school.name) > 0:
                return True

        return False

    @property
    def profile_picture_url(self):
        """
        Returns profile picture or the default avatar's url

        :return: profile picture url

        """
        if self.profile_picture:
            return self.profile_picture.url
        else:
            return static('website/images/default_avatar.png')

    @property
    def cover_image_url(self):
        """
        Returns cover image url

        :return: cover image url

        """
        if self.cover_image:
            return self.cover_image.url

    def setup_link(self):
        """
        Creates a link for the current profile and saves it

        :return: None

        """
        profile_link_name = (self.user.first_name.strip() + '.' + self.user.last_name.strip()).lower()
        number = 1

        """
        The link for the current profile would be as following; 'firstname.lastname' however, if a user with the same
        first name and last name exists it will be as following; 'firstname.lastname.2' and if that exists as well it
        will increase the tailing number and retries until it gets saved properly.
        """
        while True:
            try:
                if number > 1:
                    self.link = profile_link_name + "." + str(number)
                else:
                    self.link = profile_link_name
                self.save()
                break
            except IntegrityError:
                number += 1

    def get_image_upload_modal_dics(self):
        """
        Returns profile and cover image's common properties

        :return:
        """
        return {
            'profile_picture': {
                'upload_url': 'upload-profile-picture/',
                'onsuccess': 'profile_ajax_functions.on_profile_picture_success',
                'onerror': 'profile_ajax_functions.on_profile_picture_error',
                'id': 'profile_picture',
                'name': 'profile_picture',
                'apply': 'profile_picture_apply',
                'form_name': 'profile_picture_form',
                'title': 'Change your profile photo',
                'modal_id': 'profile_picture_modal'
            },
            'cover_image': {
                'upload_url': 'upload-cover-image/',
                'onsuccess': 'profile_ajax_functions.on_cover_image_success',
                'onerror': 'profile_ajax_functions.on_cover_image_error',
                'id': 'cover_image',
                'name': 'cover_image',
                'apply': 'cover_image_apply',
                'form_name': 'cover_image_form',
                'title': 'Change your cover image',
                'modal_id': 'cover_image_modal'
            }

        }

    def get_profile_overview_dict(self):
        """
        Formats a profile card for the current profile and returns it.

        :return: Dictionary fo formatted profile card

        """
        output = {
            'name': self.user.first_name + " " + self.user.last_name,
            'profile_picture': self.profile_picture_url,
            'cover_image': self.cover_image_url,
            'overall_gpa': self.student.overall_gpa,
            'profile_link': self.link,
            'pledges': self.student.get_pledge_view_info(),
            'user_id': self.user.pk
        }

        try:
            college = self.student.colleges.get(order=1)
            if college.name:
                output['college_name'] = college.name
                output['major'] = college.major_name
                output['degree'] = college.degree.name if college.degree else ""
            else:
                high_school = self.student.high_schools.get(order=1)
                output['college_name'] = high_school.name
                output['major'] = ""
                output['degree'] = ""
        except StudentCollege.DoesNotExist:
            output['college_name'] = ""
            output['major'] = ""
            output['degree'] = ""

        return output

    def get_comments(self, logged_in_user_id):
        """
        Formats and returns all comments for the current user profile

        :return: Array of dictionaries

        """
        formatted_comments = []
        for comment in self.commentee.filter(is_removed=False):

            try:
                amount = comment.contribution.amount
            except Contribution.DoesNotExist:
                amount = None

            if logged_in_user_id and comment.commenter.user.pk == logged_in_user_id:
                is_editable = True
            else:
                is_editable = False

            formatted_comments.append({
                'comment_id': comment.pk,
                'datetime': comment.created_timezoned.strftime(Constants.FULL_DATETIME_DISPLAY),
                'commenter': comment.commenter.user.get_full_name(),
                'comment': comment.comment,
                'profile_url': comment.commenter.link,
                'profile_picture': comment.commenter.profile_picture_url,
                'is_own_comment': comment.is_own_comment,
                'amount': amount,
                'is_editable': is_editable
            })

        return formatted_comments

    def get_profile_view_dict(self):
        """
        Formats and returns a detailed dictionary of the current user profile

        :return: Dictionary containing profile details

        """
        try:
            self.student
        except Student.DoesNotExist:
            return None

        output = {
            'grad_story': self.student.grad_story,
            'hobbies': self.student.hobbies,
            'volunteer_projects_internships': self.student.volunteer_projects_internships,
            'more_about_me': self.student.more_about_me
        }

        try:
            if self.student.is_high_school_student:
                high_school = self.student.high_schools.get(order=1)
                output['high_school'] = {
                    'name': high_school.name,
                    'graduation_date': high_school.end_date.strftime(Constants.MONTH_YEAR_DISPLAY_DATE_FORMAT),
                    'projects': high_school.projects,
                    'story': high_school.story
                }
        except StudentHighSchool.DoesNotExist:
            pass

        for college in self.student.colleges.all():
            order = college.order
            output['college_'+str(order)] = {
                'name': college.name,
                'start_date': college.start_date.strftime(Constants.MONTH_YEAR_DISPLAY_DATE_FORMAT) if college.start_date else "",
                'graduation_date': college.end_date.strftime(Constants.MONTH_YEAR_DISPLAY_DATE_FORMAT) if college.end_date else "",
                'degree': college.degree.name if college.degree else "",
                'major': college.major_name,
                'subjects': college.subjects,
                'projects': college.projects,
                'story': college.story,
            }

        return output


class Student(models.Model):
    """
    Students model

    """
    user_profile = models.OneToOneField(UserProfile, related_name='student')
    grad_story = models.TextField(max_length=5000, null=True, blank=True, default=None)
    hobbies = models.TextField(max_length=5000, null=True, blank=True, default=None)
    overall_gpa = models.CharField(max_length=45, null=True, blank=True, default=None)
    is_high_school_student = models.BooleanField(default=False)
    is_college_student = models.BooleanField(default=False)
    volunteer_projects_internships = models.TextField(max_length=5000, null=True, blank=True, default=None)
    more_about_me = models.TextField(max_length=5000, null=True, blank=True, default=None)
    goal_amount = models.FloatField(default=0)
    is_featured = models.BooleanField(default=False)
    display_order = models.PositiveIntegerField(null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_student'

    def __str__(self):
        return "{name}, email: {email}".format(
            name=self.user_profile.user.first_name + " " + self.user_profile.user.last_name,
            email=self.user_profile.user.email
        )

    @property
    def has_uploaded_loan_documents(self):
        """
        Specifies whether or not the student has uploaded loan documents

        :return: Boolean

        """
        for loan in self.loans.all():
            if loan.is_loan_statement_uploaded:
                return True
        return False

    @property
    def are_loan_documents_verified(self):
        """
        Specifies whether or not loan documents are verified for the current student

        :return: Boolean

        """
        if self.loans.all().count() == 0:
            return False

        for loan in self.loans.all():
            if not loan.is_verified:
                return False

        return True

    def save(self, *args, **kwargs):
        """
        Saves the student and sets its default goal amount

        :param args:
        :param kwargs:
        :return:
        """
        if self.pk is None and self.goal_amount == 0:
            self.goal_amount = Constants.DEFAULT_GOAL_AMOUNT
        super(Student, self).save(*args, **kwargs)

    def sort_colleges(self):
        """
        Sorts student colleges based on their end date, start date and degree and saves them

        :return:

        """
        student_colleges = list(self.colleges.all())
        try:
            student_colleges.sort(key=lambda item: item.degree.order, reverse=True)
        except AttributeError:
            pass

        try:
            student_colleges.sort(key=lambda item: item.start_date, reverse=True)
        except TypeError:
            pass

        try:
            student_colleges.sort(key=lambda item: item.end_date, reverse=True)
        except TypeError:
            pass

        for college in self.colleges.all():
            college.order = None
            college.save()

        for i in range(0, len(student_colleges)):
            college = self.colleges.get(pk=student_colleges[i].pk)
            college.order = i + 1
            college.save()

    def get_pledge_view_info(self, backer=None):
        """
        Formats and returns pledge view information for the student

        :param backer:
        :return: Dictionary containing student's pledge info

        """
        total_pledged_amount = 0

        if backer:
            pledges = self.donations_received.filter(backer=backer)
        else:
            pledges = self.donations_received.all()

        for pledge in pledges:
            total_pledged_amount += pledge.amount

        if backer:
            number_of_pledges = self.donations_received.filter(backer=backer).values_list('backer',
                                                                                           flat=True).distinct().count()
        else:
            number_of_pledges = self.donations_received.filter(backer=None).count(
            ) + self.donations_received.exclude(backer=None).values_list('backer', flat=True).distinct().count()

        try:
            percentage_pledged = int((float(total_pledged_amount) / float(self.goal_amount)) * 100)
        except ZeroDivisionError:
            if float(total_pledged_amount) > 0:
                percentage_pledged = 100
            else:
                percentage_pledged = 0

        return {
            'goal_amount': self.goal_amount,
            'total_pledged_amount': total_pledged_amount,
            'number_of_pledges': number_of_pledges,
            'percentage_pledged': percentage_pledged
        }

    def get_all_loan_documents(self):
        """
        Formats and returns all loan documents for the student

        :return: Array of dictionary

        """
        loan_documents = []
        for loan in self.loans.all():
            for document in loan.documents.all():
                if os.path.isfile(document.document.path):
                    loan_documents.append({
                        'filename': document.filename,
                        'datetime': document.uploaded_datetime.strftime(Constants.FULL_DATETIME_DISPLAY),
                        'is_verified': document.is_verified
                    })
        return loan_documents

    def send_welcome_email(self):
        context = {
            'full_name': self.user_profile.user.get_full_name(),
            'student_link': settings.BASE_URL + reverse('profile', args=(self.user_profile.link,)),
            'contact_us': settings.BASE_URL + reverse('contact'),
            'logo_url': settings.BASE_URL + static('website/images/gradlift@2x.png')
        }

        tasks.send_mail.delay(
            subject=Constants.EMAIL_SUBJECT_WELCOME,
            to=(self.user_profile.user.email, settings.EMAIL_ADMIN_NOTIFY_USER_JOINED,),
            html_template='email_templates/welcome.html',
            context=context
        )


class Backer(models.Model):
    user_profile = models.OneToOneField(UserProfile, related_name='backer')

    class Meta:
        db_table = 'data_backer'

    def __str__(self):
        return "{name}, email: {email}".format(
            name=self.user_profile.user.first_name + " " + self.user_profile.user.last_name,
            email=self.user_profile.user.email
        )

    def get_pledge_view_info(self, student=None):

        if student:
            total_pledged_amount = self.contributions.filter(student=student).aggregate(Sum('amount'))
        else:
            total_pledged_amount = self.contributions.aggregate(Sum('amount'))

        number_of_students_pledged = self.contributions.all().values_list('student', flat=True).distinct().count()

        if student:
            number_of_pledges_for_student = self.contributions.filter(student=student).count()
        else:
            number_of_pledges_for_student = self.contributions.all().count()

        return {
            'total_pledged_amount': total_pledged_amount['amount__sum'],
            'number_of_students_pledged': number_of_students_pledged,
            'number_of_pledges_for_student': number_of_pledges_for_student
        }


class StudentLoan(models.Model):
    student = models.ForeignKey(Student, related_name='loans')
    amount_owed = models.FloatField(default=0)
    is_loan_statement_uploaded = models.BooleanField(default=False)
    loan_provider = models.CharField(max_length=45, default=None, null=True, blank=True)

    class Meta:
        db_table = 'data_student_loan'
        ordering = ["-pk"]

    def __str__(self):
        return "load for student {student_id}: {amount_owed}".format(
            student_id=self.student.user_profile.pk,
            amount_owed=self.amount_owed
        )

    @property
    def is_verified(self):

        documents = self.documents.all()

        if documents.count() == 0:
            return False

        for document in documents:
            if not document.is_verified:
                return False

        return True


def get_loan_document_upload_path(instance, filename):
    return os.path.join('loan_documents', instance.student_loan.student.user_profile.link, filename)

fs = FileSystemStorage(location=settings.PRIVATE_FILE_UPLOAD_DIRECTORY)


class StudentLoanDocument(models.Model):
    student_loan = models.ForeignKey(StudentLoan, related_name='documents')
    document = models.FileField(upload_to=get_loan_document_upload_path, storage=fs)
    uploaded_datetime = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_verified = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_student_loan_document'
        ordering = ["-uploaded_datetime"]

    @property
    def filename(self):
        return os.path.basename(self.document.name)


class StudentAccount529(models.Model):
    student = models.OneToOneField(Student, related_name='account_529')
    parent_full_name = models.CharField(max_length=254)
    parent_phone_number = models.CharField(max_length=45)
    parent_email = models.EmailField(null=True, blank=True, default=None)
    bank_name = models.CharField(max_length=254)
    full_name_on_account = models.CharField(max_length=254)
    account_number = models.CharField(max_length=254)

    class Meta:
        db_table = 'data_student_account_529'

    def __str__(self):
        return "529 account for student {student_id}: {bank_name}".format(
            student_id=self.student.user_profile.pk,
            bank_name=self.bank_name
        )


class StudentTuition(models.Model):
    student = models.ForeignKey(Student, related_name='tuitions')
    amount = models.FloatField(default=0)
    creation_date = models.DateField(null=True, blank=True, default=None)
    end_date = models.DateField(null=True, blank=True, default=None)
    description = models.TextField(max_length=5000, null=True, blank=True, default=None)
    is_paid = models.BooleanField(default=False)
    paid_date = models.DateField(null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_student_tuition'

    def __str__(self):
        return "Tuition for student {student_id}: {amount}".format(
            student_id=self.student.user_profile.pk,
            amount=self.amount
        )


class OrganizationContact(models.Model):
    phone_number = models.CharField(max_length=45)
    user = models.ForeignKey(User, null=True, blank=True, default=None)
    organization = models.ForeignKey(Organization, related_name='contacts')

    class Meta:
        db_table = 'data_organization_contact'

    def __str__(self):
        return "Organization Contact {name} for {organization}".format(
            name=self.user.first_name + " " + self.user.last_name,
            organization=self.organization.name
        )


class Institution(models.Model):
    unit_id = models.IntegerField(null=True, blank=True, default=None)
    name = models.CharField(max_length=254)
    phone_number = models.CharField(max_length=45, null=True, blank=True, default=None)
    fax_number = models.CharField(max_length=45, null=True, blank=True, default=None)
    website = models.URLField(null=True, blank=True, default=None)
    zip_code = models.CharField(max_length=45, null=True, blank=True, default=None)
    city = models.CharField(max_length=254, null=True, blank=True, default=None)
    street = models.CharField(max_length=500, null=True, blank=True, default=None)
    state = models.ForeignKey(RefState, null=True, blank=True, default=None)
    country = models.ForeignKey(RefCountry, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_institution'

    def __str__(self):
        return self.name


class StudentCollege(models.Model):
    student = models.ForeignKey(Student, related_name='colleges')
    name = models.CharField(max_length=254, null=True, blank=True, default=None)
    order = models.SmallIntegerField(null=True, blank=True, default=None)
    institution = models.ForeignKey(Institution, null=True, blank=True, default=None)
    start_date = models.DateField(null=True, blank=True, default=None)
    end_date = models.DateField(null=True, blank=True, default=None)
    season = models.ForeignKey(RefSeason, null=True, blank=True, default=None)
    degree = models.ForeignKey(RefDegree, null=True, blank=True, default=None)
    major = models.ForeignKey(RefMajor, null=True, blank=True, default=None)
    major_name = models.CharField(max_length=254, null=True, blank=True, default=None)
    subjects = models.TextField(max_length=5000, null=True, blank=True, default=None)
    projects = models.TextField(max_length=5000, null=True, blank=True, default=None)
    story = models.TextField(max_length=5000, null=True, blank=True, default=None)
    is_in_progress = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_student_college'
        unique_together = ('order', 'student',)
        ordering = ["-order"]

    def __str__(self):
        return "Student College for student {student_name} and college {college_name}".format(
            student_name=self.student.user_profile.user.first_name + " " + self.student.user_profile.user.last_name,
            college_name=self.name
        )


class StudentHighSchool(models.Model):
    student = models.ForeignKey(Student, related_name='high_schools')
    name = models.CharField(max_length=45, null=True, blank=True, default=None)
    order = models.SmallIntegerField(default=1)
    institution = models.ForeignKey(Institution, null=True, blank=True, default=None)
    start_date = models.DateField(null=True, blank=True, default=None)
    end_date = models.DateField(null=True, blank=True, default=None)
    subjects = models.TextField(max_length=5000, null=True, blank=True, default=None)
    projects = models.TextField(max_length=5000, null=True, blank=True, default=None)
    story = models.TextField(max_length=5000, null=True, blank=True, default=None)
    is_in_progress = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_student_high_school'
        unique_together = ('order', 'student',)

    def __str__(self):
        return "Student High School for student {student_name} and high school {high_school_name}".format(
            student_name=self.student.user_profile.user.first_name + " " + self.student.user_profile.user.last_name,
            high_school_name=self.name
        )


class Comment(models.Model):
    commenter = models.ForeignKey(UserProfile, related_name='commenter', null=True, blank=True, default=None)
    commentee = models.ForeignKey(UserProfile, related_name='commentee', null=True, blank=True, default=None)
    comment = models.TextField(max_length=500, null=True, blank=True, default=None)
    created = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_notification_sent = models.BooleanField(default=False)
    is_removed = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_comment'
        ordering = ['-created']

    def __str__(self):
        return "{commenter} commented for {commentee}".format(
            commenter=self.commenter.user.get_full_name(),
            commentee=self.commentee.user.get_full_name(),
        )

    @property
    def created_timezoned(self):
        return self.created.astimezone(timezone(settings.TIME_ZONE))

    @property
    def is_own_comment(self):
        return self.commentee == self.commenter


class StripeCustomerPayment(models.Model):
    user = models.ForeignKey(User, related_name='customer_payments', null=True, blank=True, default=None)
    stripe_customer_id = models.CharField(max_length=500, null=True, blank=True, default=None)
    is_most_recent = models.BooleanField(default=True)
    client_ip = models.GenericIPAddressField(null=True, blank=True, default=None)
    stripe_token = models.CharField(max_length=500, null=True, blank=True, default=None)
    created = models.DateTimeField(null=True, blank=True, default=None)
    email = models.EmailField(null=True, blank=True, default=None)
    address_city = models.CharField(max_length=254, null=True, blank=True, default=None)
    address_country = models.CharField(max_length=45, null=True, blank=True, default=None)
    address_street = models.CharField(max_length=500, null=True, blank=True, default=None)
    address_state = models.CharField(max_length=45, null=True, blank=True, default=None)
    address_zip_code = models.CharField(max_length=45, null=True, blank=True, default=None)
    brand = models.CharField(max_length=500, null=True, blank=True, default=None)
    country = models.CharField(max_length=500, null=True, blank=True, default=None)
    cvc_check = models.CharField(max_length=500, null=True, blank=True, default=None)
    expiration_month = models.SmallIntegerField(null=True, blank=True, default=None)
    expiration_year = models.SmallIntegerField(null=True, blank=True, default=None)
    funding = models.CharField(max_length=500, null=True, blank=True, default=None)
    card_id = models.CharField(max_length=500, null=True, blank=True, default=None)
    last_4_digits = models.CharField(max_length=500, null=True, blank=True, default=None)
    name = models.CharField(max_length=500, null=True, blank=True, default=None)
    fingerprint = models.CharField(max_length=500, null=True, blank=True, default=None)

    class Meta:
        db_table = 'data_stripe_customer_payment'
        ordering = []


class ContributionManager(models.Manager):
    def make_contribution(self, student_user_id, amount, stripe_customer_payment,
                          is_recurring, contribution_type, backer_user_id=None):
        student = Student.objects.get(user_profile__user__pk=student_user_id)
        backer = None
        if backer_user_id:
            try:
                backer = Backer.objects.get(user_profile__user__pk=backer_user_id)
            except Backer.DoesNotExist:
                backer_user_profile = UserProfile.objects.get(user__pk=backer_user_id)
                backer = Backer(user_profile=backer_user_profile)
                backer.save()

        contribution = Contribution(student=student,
                                    backer=backer,
                                    amount=float(float(amount)/100),
                                    stripe_customer_payment=stripe_customer_payment,
                                    is_recurring=is_recurring,
                                    contribution_type=contribution_type)

        try:
            stripe.Charge.create(
                amount=amount,
                currency="usd",
                customer=stripe_customer_payment.stripe_customer_id
            )
        except stripe.error.CardError as e:
            message = str(e)
            contribution.is_completed = False
            contribution.has_error = True
            contribution.error_message = message
            logger.error(message)
            return False, message

        contribution.is_completed = True
        contribution.has_error = False
        contribution.save()

        context = {
            'amount': "{0:.2f}".format(float(amount)/100),
            'student_name': student.user_profile.user.get_full_name(),
            'backer_name': stripe_customer_payment.name,
            'student_email_address': student.user_profile.user.email,
            'backer_email_address': stripe_customer_payment.email,
            'date': stripe_customer_payment.created.strftime(Constants.FULL_DATETIME_DISPLAY),
            'student_link': settings.BASE_URL + reverse('profile', args=(student.user_profile.link,)),
            'contact_us': settings.BASE_URL + reverse('contact'),
            'logo_url': settings.BASE_URL + static('website/images/gradlift@2x.png')
        }

        tasks.send_mail.delay(
            subject=Constants.EMAIL_SUBJECT_BACKER_RECEIPT,
            to=(stripe_customer_payment.email,),
            html_template='email_templates/backer_receipt.html',
            context=context
        )

        tasks.send_mail.delay(
            subject=Constants.EMAIL_SUBJECT_STUDENT_RECEIPT,
            to=(student.user_profile.user.email,),
            html_template='email_templates/student_receipt.html',
            context=context
        )

        return True, None

    def get_dicts(self):
        dicts = []
        for contribution in self.all():
            dicts.append(
                {
                    'amount': contribution.amount,
                    'backer_name': contribution.backer.user_profile.user.get_full_name() if contribution.backer else 'Anonymous',
                    'backer_profile_link': contribution.backer.user_profile.link if contribution.backer else None,
                    'backer_profile_picture': contribution.backer.user_profile.profile_picture_url if contribution.backer else static('website/images/default_avatar.png'),
                    'student_name': contribution.student.user_profile.user.get_full_name(),
                    'student_profile_link': contribution.student.user_profile.link,
                    'student_profile_picture': contribution.student.user_profile.profile_picture_url,
                    'creation_date': contribution.creation_datetime.strftime(Constants.FULL_DATE_DISPLAY),
                    'comment': contribution.comment.comment if contribution.comment else None,
                    'pledge_info': contribution.backer.get_pledge_view_info(contribution.student) if contribution.backer else None,
                    'backer_pledge_info': contribution.student.get_pledge_view_info(contribution.backer) if contribution.backer else None,
                }
            )
        return dicts


class Contribution(models.Model):
    student = models.ForeignKey(Student, related_name='donations_received')
    backer = models.ForeignKey(Backer, related_name='contributions', null=True, blank=True, default=None)
    amount = models.FloatField(default=0)
    creation_datetime = models.DateTimeField(auto_now=True, null=True, blank=True)
    contribution_type = models.ForeignKey(RefContributionType, null=True, blank=True, default=None)
    is_completed = models.BooleanField(default=False)
    stripe_customer_payment = models.ForeignKey(StripeCustomerPayment, related_name='contributions', null=True, blank=True, default=None)
    is_recurring = models.BooleanField(default=False)
    comment = models.OneToOneField(Comment, related_name='contribution', null=True, blank=True, default=None)
    has_error = models.BooleanField(default=False)
    error_message = models.CharField(max_length=500, null=True, blank=True, default=None)

    objects = ContributionManager()

    class Meta:
        db_table = 'data_contribution'
        ordering = ['-creation_datetime']

    def __str__(self):
        if self.backer:
            backer_name = self.backer.user_profile.user.first_name + " " + self.backer.user_profile.user.last_name
        else:
            backer_name = "Anonymous Backer"
        return "Contribution from {backer_name} to {student_name}".format(
            backer_name=backer_name,
            student_name=self.student.user_profile.user.first_name + " " + self.student.user_profile.user.last_name,
        )


class ProfileSocialNetwork(models.Model):
    user_profile = models.ForeignKey(UserProfile, related_name='social_networks')
    social_network_type = models.ForeignKey(RefSocialNetworkType)
    is_connected = models.BooleanField(default=False)
    link = models.CharField(max_length=500)

    class Meta:
        db_table = 'data_profile_social_network'

    def __str__(self):
        return "Profile social network for {name} for {social_network_type}".format(
            name=self.user_profile.user.first_name + " " + self.user_profile.user.last_name,
            social_network_type=self.social_network_type.name,
        )


class Notification(models.Model):
    description = models.TextField(max_length=50000, null=True, blank=True, default=None)
    creation_datetime = models.DateTimeField(auto_now=True, null=True, blank=True)
    type = models.ForeignKey(RefNotificationType, null=True, blank=True, default=None)
    student = models.ForeignKey(Student, null=True, blank=True, default=None)
    is_read = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_notification'
        ordering = ['-is_read', '-creation_datetime', '-student']


class Contact(models.Model):
    user_profile = models.ForeignKey(UserProfile, null=True, blank=True, default=None)
    name = models.CharField(max_length=45)
    email = models.EmailField()
    subject = models.CharField(max_length=500)
    message = models.TextField(max_length=5000)
    created = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        db_table = 'data_contact'
        ordering = ['-is_read', '-created']

    def send_email(self):
        message_body = "subject: {subject}\nname: {name}\nemail: {email}\nmessage: {message}\ndate: {date}".format(
            subject=self.subject,
            name=self.name,
            message=self.message,
            email=self.email,
            date=self.created.strftime(Constants.FULL_DATETIME_DISPLAY),
        )

        tasks.send_mail.delay(subject="Contact Message - {subject}".format(subject=self.subject),
                              body=message_body,
                              from_email=settings.EMAIL_HOST_USER,
                              to=(settings.WE_CARE_EMAIL_USER,),
                              reply_to=(self.email,))

    def save(self, *args, **kwargs):
        """
        Saves the profile and if it doesn't have a specific link, it creates one for it.

        :param args:
        :param kwargs:
        :return: None

        """
        super(Contact, self).save(*args, **kwargs)
        self.send_email()


class BlogManager(models.Manager):

    NUMBER_OF_BLOGS_PER_ROW = 3

    def get_most_recent_blogs(self, max_number=None):
        now = datetime.datetime.now()
        blogs = self.filter(lunch_datetime__lte=now)

        if max_number:
            blogs = blogs[:max_number]

        formatted_blogs = []
        for blog in blogs:
            formatted_blogs.append(blog.get_formatted_blog())

        formatted_blogs_iter = iter(formatted_blogs)
        complete_list = [list(islice(formatted_blogs_iter, BlogManager.NUMBER_OF_BLOGS_PER_ROW)) for _ in xrange(
            len(formatted_blogs)/BlogManager.NUMBER_OF_BLOGS_PER_ROW)]

        reminder_of_list = len(formatted_blogs) % BlogManager.NUMBER_OF_BLOGS_PER_ROW

        if reminder_of_list != 0:
            complete_list.append(formatted_blogs[-reminder_of_list:])

        return complete_list

    def get_blog_by_link(self, blog_link):
        try:
            blog = self.get(link=blog_link)
        except Blog.DoesNotExist:
            return {}
        return blog.get_formatted_blog()


class Blog(models.Model):
    title = models.CharField(max_length=500)
    writer_name = models.CharField(max_length=500)
    image = models.ImageField(upload_to='blog-images/%Y/%m/', null=True, blank=True)
    summary = models.TextField(max_length=50000)
    content = models.TextField(max_length=50000)
    link = models.SlugField(max_length=255, unique=True)
    lunch_datetime = models.DateTimeField(null=True, blank=True)
    created = models.DateTimeField(auto_now=True, null=True, blank=True)

    objects = BlogManager()

    class Meta:
        db_table = 'data_blog'
        ordering = ['-lunch_datetime']

    def get_formatted_blog(self):
        return {
            'link': reverse('blog', args=(self.link,)),
            'image_url': self.image.url if self.image else None,
            'writer_name': self.writer_name,
            'title': self.title,
            'content': self.content,
            'summary': self.summary,
            'lunch_datetime': self.lunch_datetime_timezoned.strftime(Constants.FULL_DATETIME_DISPLAY)
        }

    @property
    def lunch_datetime_timezoned(self):
        return self.lunch_datetime.astimezone(timezone(settings.TIME_ZONE))
