from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.forms import PasswordChangeForm

from gradlift.apps import Constants
from gradlift.apps.data import validators
from gradlift.apps.data import models


class AccountGeneralForm(forms.Form):

    STATE_CHOICES = models.RefState.objects.get_options()
    COUNTRY_CHOICES = models.RefCountry.objects.get_options()

    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    referral_code = forms.CharField(required=False, validators=[validators.validate_referral_code])
    birthday = forms.DateField(required=False, input_formats=[Constants.MONTH_DAY_YEAR_NORMAL_FORMAT])
    street = forms.CharField(required=False)
    city = forms.CharField(required=False)
    state = forms.ChoiceField(choices=STATE_CHOICES, required=False)
    zip_code = forms.CharField(required=False)
    country = forms.ChoiceField(choices=COUNTRY_CHOICES, required=False)

    notification_frequency = forms.ChoiceField(choices=models.UserProfile.NOTIFICATION_CHOICES,
                                               required=True)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AccountGeneralForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        user = self.request.user
        email = self.cleaned_data['email']
        if User.objects.filter(Q(email=email) | Q(username=email)).exists():
            multiple_records_returned = False
            try:
                email_user = User.objects.get(Q(email=email) | Q(username=email))
            except User.MultipleObjectsReturned:
                multiple_records_returned = True

            if multiple_records_returned or (email_user.pk != user.pk):
                raise ValidationError('A user with this email already exists.')
        return email

    def clean_state(self):
        state = self.cleaned_data.get('state')
        if state:
            return models.RefState.objects.get(code=state)
        else:
            return None

    def clean_country(self):
        country = self.cleaned_data.get('country')
        if country:
            return models.RefCountry.objects.get(code=country)
        else:
            return None

    def save(self, user_id):
        user = User.objects.get(pk=user_id)

        email = self.cleaned_data['email']

        try:
            user_profile = models.UserProfile.objects.get(user=user)
        except models.UserProfile.DoesNotExist:
            user_profile = models.UserProfile(user=user)

        try:
            referral_code = models.ReferralCode.objects.get(code=self.cleaned_data['referral_code'])
        except models.ReferralCode.DoesNotExist:
            referral_code = None

        user_profile.referral_code = referral_code

        user_profile.state = self.cleaned_data['state']
        user_profile.country = self.cleaned_data['country']
        user_profile.zip_code = self.cleaned_data['zip_code']
        user_profile.city = self.cleaned_data['city']
        user_profile.street = self.cleaned_data['street']

        user_profile.birthday = self.cleaned_data['birthday']
        user_profile.notification_frequency = self.cleaned_data['notification_frequency']

        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = email
        user.username = email

        user.save()
        user_profile.save()


class AccountPasswordChangeForm(PasswordChangeForm):
    def clean_new_password1(self):
        password = self.cleaned_data.get('new_password1')
        if len(password) < Constants.PASSWORD_MIN_LENGTH:
            raise validators.ValidationError(
                'This password is too short. It must contain at least %s characters.' % Constants.PASSWORD_MIN_LENGTH)
        return password


class DisableAccountForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(DisableAccountForm, self).__init__(*args, **kwargs)

    def save(self):
        self.request.user.is_active = False
        self.request.user.save()
