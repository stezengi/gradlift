# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import gradlift.apps.data.models
import django.core.files.storage
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Backer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'data_backer',
            },
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=500)),
                ('writer_name', models.CharField(max_length=500)),
                ('image', models.ImageField(null=True, upload_to=b'blog-images/%Y/%m/', blank=True)),
                ('summary', models.TextField(max_length=50000)),
                ('content', models.TextField(max_length=50000)),
                ('link', models.SlugField(unique=True, max_length=255)),
                ('lunch_datetime', models.DateTimeField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'ordering': ['-lunch_datetime'],
                'db_table': 'data_blog',
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField(default=None, max_length=500, null=True, blank=True)),
                ('created', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'ordering': ['-created'],
                'db_table': 'data_comment',
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=45)),
                ('email', models.EmailField(max_length=254)),
                ('subject', models.CharField(max_length=500)),
                ('message', models.TextField(max_length=5000)),
                ('created', models.DateTimeField(auto_now=True, null=True)),
                ('is_read', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-is_read', '-created'],
                'db_table': 'data_contact',
            },
        ),
        migrations.CreateModel(
            name='Contribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.FloatField(default=0)),
                ('creation_datetime', models.DateTimeField(auto_now=True, null=True)),
                ('is_completed', models.BooleanField(default=False)),
                ('is_recurring', models.BooleanField(default=False)),
                ('has_error', models.BooleanField(default=False)),
                ('error_message', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('backer', models.ForeignKey(related_name='contributions', default=None, blank=True, to='data.Backer', null=True)),
                ('comment', models.OneToOneField(related_name='contribution', null=True, default=None, blank=True, to='data.Comment')),
            ],
            options={
                'ordering': ['-creation_datetime'],
                'db_table': 'data_contribution',
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('unit_id', models.IntegerField(default=None, null=True, blank=True)),
                ('name', models.CharField(max_length=254)),
                ('phone_number', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('fax_number', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('website', models.URLField(default=None, null=True, blank=True)),
                ('zip_code', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('city', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('street', models.CharField(default=None, max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_institution',
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(default=None, max_length=50000, null=True, blank=True)),
                ('creation_datetime', models.DateTimeField(auto_now=True, null=True)),
                ('is_read', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['-is_read', '-creation_datetime', '-student'],
                'db_table': 'data_notification',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=254)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
                ('phone_number', models.CharField(max_length=45)),
                ('zip_code', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('city', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('street', models.CharField(default=None, max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_organization',
            },
        ),
        migrations.CreateModel(
            name='OrganizationAgreement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(default=None, max_length=50000, null=True, blank=True)),
                ('creation_date', models.DateField(default=None, null=True, blank=True)),
                ('expiration_date', models.DateField(default=None, null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'data_organization_agreement',
            },
        ),
        migrations.CreateModel(
            name='OrganizationContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_number', models.CharField(max_length=45)),
                ('organization', models.ForeignKey(related_name='contacts', to='data.Organization')),
                ('user', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'data_organization_contact',
            },
        ),
        migrations.CreateModel(
            name='ProfileSocialNetwork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_connected', models.BooleanField(default=False)),
                ('link', models.CharField(max_length=500)),
            ],
            options={
                'db_table': 'data_profile_social_network',
            },
        ),
        migrations.CreateModel(
            name='RefContributionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
                ('order', models.SmallIntegerField(default=0)),
            ],
            options={
                'ordering': ['order'],
                'db_table': 'data_ref_contribution_type',
            },
        ),
        migrations.CreateModel(
            name='RefCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
            ],
            options={
                'db_table': 'data_ref_country',
            },
        ),
        migrations.CreateModel(
            name='RefDegree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('order', models.SmallIntegerField(default=0)),
            ],
            options={
                'ordering': ['order'],
                'db_table': 'data_ref_degree',
            },
        ),
        migrations.CreateModel(
            name='ReferralCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=254)),
                ('description', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('creation_date', models.DateField(default=None, null=True, blank=True)),
                ('expiration_date', models.DateField(default=None, null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('organization', models.ForeignKey(default=None, blank=True, to='data.Organization', null=True)),
            ],
            options={
                'db_table': 'data_referral_code',
            },
        ),
        migrations.CreateModel(
            name='RefMajor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=254)),
                ('name', models.CharField(max_length=254)),
            ],
            options={
                'db_table': 'data_ref_major',
            },
        ),
        migrations.CreateModel(
            name='RefNotificationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_ref_notification_type',
            },
        ),
        migrations.CreateModel(
            name='RefOrganizationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_ref_organization_type',
            },
        ),
        migrations.CreateModel(
            name='RefSeason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('order', models.SmallIntegerField(default=0)),
            ],
            options={
                'ordering': ['order'],
                'db_table': 'data_ref_season',
            },
        ),
        migrations.CreateModel(
            name='RefSocialNetworkType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('order', models.SmallIntegerField(default=0)),
            ],
            options={
                'ordering': ['order'],
                'db_table': 'data_ref_social_network_type',
            },
        ),
        migrations.CreateModel(
            name='RefState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
            ],
            options={
                'db_table': 'data_ref_state',
            },
        ),
        migrations.CreateModel(
            name='RefTier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
                ('integer_value', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ['integer_value'],
                'db_table': 'data_ref_tier',
            },
        ),
        migrations.CreateModel(
            name='RefUserType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=45)),
                ('name', models.CharField(max_length=45)),
                ('description', models.TextField(default=None, max_length=500, null=True, blank=True)),
            ],
            options={
                'db_table': 'data_ref_user_type',
            },
        ),
        migrations.CreateModel(
            name='StripeCustomerPayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stripe_customer_id', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('is_most_recent', models.BooleanField(default=True)),
                ('client_ip', models.GenericIPAddressField(default=None, null=True, blank=True)),
                ('stripe_token', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('created', models.DateTimeField(default=None, null=True, blank=True)),
                ('email', models.EmailField(default=None, max_length=254, null=True, blank=True)),
                ('address_city', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('address_country', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('address_street', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('address_state', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('address_zip_code', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('brand', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('country', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('cvc_check', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('expiration_month', models.SmallIntegerField(default=None, null=True, blank=True)),
                ('expiration_year', models.SmallIntegerField(default=None, null=True, blank=True)),
                ('funding', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('card_id', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('last_4_digits', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('name', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('fingerprint', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('user', models.ForeignKey(related_name='customer_payments', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': [],
                'db_table': 'data_stripe_customer_payment',
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('grad_story', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('hobbies', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('overall_gpa', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('is_high_school_student', models.BooleanField(default=False)),
                ('is_college_student', models.BooleanField(default=False)),
                ('volunteer_projects_internships', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('more_about_me', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('goal_amount', models.FloatField(default=0)),
                ('is_featured', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'data_student',
            },
        ),
        migrations.CreateModel(
            name='StudentAccount529',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('parent_full_name', models.CharField(max_length=254)),
                ('parent_phone_number', models.CharField(max_length=45)),
                ('parent_email', models.EmailField(default=None, max_length=254, null=True, blank=True)),
                ('bank_name', models.CharField(max_length=254)),
                ('full_name_on_account', models.CharField(max_length=254)),
                ('account_number', models.CharField(max_length=254)),
                ('student', models.OneToOneField(related_name='account_529', to='data.Student')),
            ],
            options={
                'db_table': 'data_student_account_529',
            },
        ),
        migrations.CreateModel(
            name='StudentCollege',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('order', models.SmallIntegerField(default=None, null=True, blank=True)),
                ('start_date', models.DateField(default=None, null=True, blank=True)),
                ('end_date', models.DateField(default=None, null=True, blank=True)),
                ('major_name', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('subjects', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('projects', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('story', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('is_in_progress', models.BooleanField(default=False)),
                ('degree', models.ForeignKey(default=None, blank=True, to='data.RefDegree', null=True)),
                ('institution', models.ForeignKey(default=None, blank=True, to='data.Institution', null=True)),
                ('major', models.ForeignKey(default=None, blank=True, to='data.RefMajor', null=True)),
                ('season', models.ForeignKey(default=None, blank=True, to='data.RefSeason', null=True)),
                ('student', models.ForeignKey(related_name='colleges', to='data.Student')),
            ],
            options={
                'ordering': ['-order'],
                'db_table': 'data_student_college',
            },
        ),
        migrations.CreateModel(
            name='StudentHighSchool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('order', models.SmallIntegerField(default=1)),
                ('start_date', models.DateField(default=None, null=True, blank=True)),
                ('end_date', models.DateField(default=None, null=True, blank=True)),
                ('subjects', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('projects', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('story', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('is_in_progress', models.BooleanField(default=False)),
                ('institution', models.ForeignKey(default=None, blank=True, to='data.Institution', null=True)),
                ('student', models.ForeignKey(related_name='high_schools', to='data.Student')),
            ],
            options={
                'db_table': 'data_student_high_school',
            },
        ),
        migrations.CreateModel(
            name='StudentLoan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount_owed', models.FloatField(default=0)),
                ('is_loan_statement_uploaded', models.BooleanField(default=False)),
                ('loan_provider', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('student', models.ForeignKey(related_name='loans', to='data.Student')),
            ],
            options={
                'ordering': ['-pk'],
                'db_table': 'data_student_loan',
            },
        ),
        migrations.CreateModel(
            name='StudentLoanDocument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('document', models.FileField(storage=django.core.files.storage.FileSystemStorage(location=b'/var/www/gradlift/private-file-uploads'), upload_to=gradlift.apps.data.models.get_loan_document_upload_path)),
                ('uploaded_datetime', models.DateTimeField(auto_now=True, null=True)),
                ('is_verified', models.BooleanField(default=False)),
                ('student_loan', models.ForeignKey(related_name='documents', to='data.StudentLoan')),
            ],
            options={
                'ordering': ['-uploaded_datetime'],
                'db_table': 'data_student_loan_document',
            },
        ),
        migrations.CreateModel(
            name='StudentTuition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.FloatField(default=0)),
                ('creation_date', models.DateField(default=None, null=True, blank=True)),
                ('end_date', models.DateField(default=None, null=True, blank=True)),
                ('description', models.TextField(default=None, max_length=5000, null=True, blank=True)),
                ('is_paid', models.BooleanField(default=False)),
                ('paid_date', models.DateField(default=None, null=True, blank=True)),
                ('student', models.ForeignKey(related_name='tuitions', to='data.Student')),
            ],
            options={
                'db_table': 'data_student_tuition',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cover_image', models.ImageField(null=True, upload_to=b'cover-images/%Y/%m/', blank=True)),
                ('profile_picture', models.ImageField(null=True, upload_to=b'profile-pictures/%Y/%m/', blank=True)),
                ('has_agreed_to_terms', models.BooleanField(default=False)),
                ('birthday', models.DateField(default=None, null=True, blank=True)),
                ('zip_code', models.CharField(default=None, max_length=45, null=True, blank=True)),
                ('city', models.CharField(default=None, max_length=254, null=True, blank=True)),
                ('street', models.CharField(default=None, max_length=500, null=True, blank=True)),
                ('link', models.CharField(max_length=500)),
                ('country', models.ForeignKey(default=None, blank=True, to='data.RefCountry', null=True)),
                ('referral_code', models.ForeignKey(default=None, blank=True, to='data.ReferralCode', null=True)),
                ('state', models.ForeignKey(default=None, blank=True, to='data.RefState', null=True)),
                ('user', models.OneToOneField(related_name='profile', null=True, default=None, blank=True, to=settings.AUTH_USER_MODEL)),
                ('user_type', models.ForeignKey(related_name='profiles', default=None, blank=True, to='data.RefUserType', null=True)),
            ],
            options={
                'db_table': 'data_user_profile',
            },
        ),
        migrations.AddField(
            model_name='student',
            name='user_profile',
            field=models.OneToOneField(related_name='student', to='data.UserProfile'),
        ),
        migrations.AddField(
            model_name='profilesocialnetwork',
            name='social_network_type',
            field=models.ForeignKey(to='data.RefSocialNetworkType'),
        ),
        migrations.AddField(
            model_name='profilesocialnetwork',
            name='user_profile',
            field=models.ForeignKey(related_name='social_networks', to='data.UserProfile'),
        ),
        migrations.AddField(
            model_name='organizationagreement',
            name='tier',
            field=models.ForeignKey(default=None, blank=True, to='data.RefTier', null=True),
        ),
        migrations.AddField(
            model_name='organization',
            name='agreement',
            field=models.OneToOneField(null=True, default=None, blank=True, to='data.OrganizationAgreement'),
        ),
        migrations.AddField(
            model_name='organization',
            name='country',
            field=models.ForeignKey(default=None, blank=True, to='data.RefCountry', null=True),
        ),
        migrations.AddField(
            model_name='organization',
            name='state',
            field=models.ForeignKey(default=None, blank=True, to='data.RefState', null=True),
        ),
        migrations.AddField(
            model_name='organization',
            name='type',
            field=models.ForeignKey(default=None, blank=True, to='data.RefOrganizationType', null=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='student',
            field=models.ForeignKey(default=None, blank=True, to='data.Student', null=True),
        ),
        migrations.AddField(
            model_name='notification',
            name='type',
            field=models.ForeignKey(default=None, blank=True, to='data.RefNotificationType', null=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='country',
            field=models.ForeignKey(default=None, blank=True, to='data.RefCountry', null=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='state',
            field=models.ForeignKey(default=None, blank=True, to='data.RefState', null=True),
        ),
        migrations.AddField(
            model_name='contribution',
            name='contribution_type',
            field=models.ForeignKey(default=None, blank=True, to='data.RefContributionType', null=True),
        ),
        migrations.AddField(
            model_name='contribution',
            name='stripe_customer_payment',
            field=models.ForeignKey(related_name='contributions', default=None, blank=True, to='data.StripeCustomerPayment', null=True),
        ),
        migrations.AddField(
            model_name='contribution',
            name='student',
            field=models.ForeignKey(related_name='donations_received', to='data.Student'),
        ),
        migrations.AddField(
            model_name='contact',
            name='user_profile',
            field=models.ForeignKey(default=None, blank=True, to='data.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='comment',
            name='commentee',
            field=models.ForeignKey(related_name='commentee', default=None, blank=True, to='data.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='comment',
            name='commenter',
            field=models.ForeignKey(related_name='commenter', default=None, blank=True, to='data.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='backer',
            name='user_profile',
            field=models.OneToOneField(related_name='backer', to='data.UserProfile'),
        ),
        migrations.AlterUniqueTogether(
            name='studenthighschool',
            unique_together=set([('order', 'student')]),
        ),
        migrations.AlterUniqueTogether(
            name='studentcollege',
            unique_together=set([('order', 'student')]),
        ),
    ]
