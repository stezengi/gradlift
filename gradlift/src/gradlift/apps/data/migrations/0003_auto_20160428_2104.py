# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0002_initial_reference_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='is_notification_sent',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='notification_frequency',
            field=models.CharField(default=b'instantaneously', max_length=100, choices=[(b'instantaneously', b'Instantaneously'), (b'daily', b'Daily'), (b'weekly', b'Weekly')]),
        ),
    ]
