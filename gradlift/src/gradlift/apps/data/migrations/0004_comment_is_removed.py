# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_auto_20160428_2104'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='is_removed',
            field=models.BooleanField(default=False),
        ),
    ]
