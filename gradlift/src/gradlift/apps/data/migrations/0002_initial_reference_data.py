# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management import call_command

from django.db import migrations, models


class Migration(migrations.Migration):

    def load_data(apps, schema_editor):
        call_command("loaddata", "ref_states_initial_data.json")
        call_command("loaddata", "ref_countries_initial_data.json")
        call_command("loaddata", "institutions_initial_data.json")
        call_command("loaddata", "ref_degrees_initial_data.json")
        call_command("loaddata", "ref_majors_initial_data.json")
        call_command("loaddata", "ref_seasons_initial_data.json")
        call_command("loaddata", "ref_social_network_type_initial_data.json")
        call_command("loaddata", "ref_notification_type_initial_data.json")
        call_command("loaddata", "ref_contribution_type_initial_data.json")

    dependencies = [
        ('data', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data),
    ]
