from django import forms
from django.contrib.auth.models import User   # fill in custom user info then save it
from django.contrib.auth import forms as auth_forms
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from gradlift.apps import Constants
from gradlift.apps.data import validators
from gradlift.apps.data import models
from gradlift.apps.data import tasks


class RegistrationForm(auth_forms.UserCreationForm):
    email = forms.EmailField(required=True, validators=[validators.validate_email])
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    referral_code = forms.CharField(required=False, validators=[validators.validate_referral_code])

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.username = self.cleaned_data['email']

        if commit:
            user.save()

        user_profile = models.UserProfile(user=user)

        try:
            referral_code = models.ReferralCode.objects.get(code=self.cleaned_data['referral_code'])
        except models.ReferralCode.DoesNotExist:
            referral_code = None

        user_profile.referral_code = referral_code

        if commit:
            user_profile.save()

        student = models.Student(user_profile=user_profile)

        if commit:
            student.save()

        student.send_welcome_email()

        return user

    def clean_password1(self):
        password = self.cleaned_data.get('password1')
        if len(password) < Constants.PASSWORD_MIN_LENGTH:
            raise validators.ValidationError(
                'This password is too short. It must contain at least %s characters.' % Constants.PASSWORD_MIN_LENGTH)
        return password


class LoginForm(auth_forms.AuthenticationForm):
    user = None

    error_messages = {
        'invalid_login': _("Please enter a correct email and password. "),
        'inactive': _("This account is inactive."),
    }

    def confirm_login_allowed(self, user):
        super(LoginForm, self).confirm_login_allowed(user=user)
        self.user = user

    def clean(self):
        try:
            username = self.cleaned_data.get('username')
            validate_email(username)
        except ValidationError:
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                code='invalid_login',
            )
        return super(LoginForm, self).clean()


class PasswordResetForm(auth_forms.PasswordResetForm):

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if not User.objects.filter(email=email).exists():
            raise validators.ValidationError(
                'There is no user associated with this email address.'
            )

        return email


class SetPasswordForm(auth_forms.SetPasswordForm):

    def clean_new_password1(self):
        new_password1 = self.cleaned_data.get('new_password1')
        if len(new_password1) < Constants.PASSWORD_MIN_LENGTH:
            raise validators.ValidationError(
                'This password is too short. It must contain at least %s characters.' % RegistrationForm.PASSWORD_MIN_LENGTH)
        return new_password1


class ActivityForm(forms.Form):

    message = forms.CharField(widget=forms.Textarea, max_length=5000, required=True)
    user_id = forms.CharField(widget=forms.HiddenInput())

    def save(self, request):
        user_profile = models.UserProfile.objects.get(user__pk=self.cleaned_data.get('user_id'))

        comment = models.Comment(
            comment=self.cleaned_data.get('message'),
            commentee=user_profile,
            commenter=request.user.profile)

        comment.save()

        if user_profile.notification_frequency == models.UserProfile.NOTIFICATION_CHOICE_INSTANTANEOUSLY:
            tasks.send_notification_email.delay(comment_id=comment.pk)


class CommentDeleteForm(forms.Form):

    comment_id = forms.IntegerField(required=True)
    commentee_user_id = forms.IntegerField(required=True)


class UserProfileForm(forms.Form):

    DEGREE_CHOICES = models.RefDegree.objects.get_options()
    DATE_MONTH_YEAR_FORMAT = [Constants.MONTH_YEAR_DATE_FORMAT]

    grad_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    overall_gpa = forms.CharField(max_length=10, required=False)
    hobbies = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    cover_image = forms.ImageField(required=False)
    profile_picture = forms.ImageField(required=False)

    is_high_school_student = forms.ChoiceField(widget=forms.RadioSelect, choices=(('1', 'Yes',), ('0', 'No',)))
    high_school_name = forms.CharField(required=False)
    high_school_graduation_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    high_school_projects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    high_school_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    college_1_name = forms.CharField(required=False)
    college_1_start_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_1_end_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_1_degree = forms.ChoiceField(choices=DEGREE_CHOICES, required=False)
    college_1_major = forms.CharField(required=False)
    college_1_subjects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_1_projects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_1_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    attends_college_2 = forms.BooleanField(widget=forms.HiddenInput, required=False)
    college_2_name = forms.CharField(required=False)
    college_2_start_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_2_end_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_2_degree = forms.ChoiceField(choices=DEGREE_CHOICES, required=False)
    college_2_major = forms.CharField(required=False)
    college_2_subjects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_2_projects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_2_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    attends_college_3 = forms.BooleanField(widget=forms.HiddenInput, required=False)
    college_3_name = forms.CharField(required=False)
    college_3_start_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_3_end_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_3_degree = forms.ChoiceField(choices=DEGREE_CHOICES, required=False)
    college_3_major = forms.CharField(required=False)
    college_3_subjects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_3_projects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_3_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    attends_college_4 = forms.BooleanField(widget=forms.HiddenInput, required=False)
    college_4_name = forms.CharField(required=False)
    college_4_start_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_4_end_date = forms.DateField(required=False, input_formats=DATE_MONTH_YEAR_FORMAT)
    college_4_degree = forms.ChoiceField(choices=DEGREE_CHOICES, required=False)
    college_4_major = forms.CharField(required=False)
    college_4_subjects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_4_projects = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    college_4_story = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    volunteer_projects_internships = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)
    more_about_me = forms.CharField(widget=forms.Textarea, max_length=5000, required=False)

    def required_if_field_true(self, current_field, field):
        value = self.cleaned_data.get(current_field, '')
        if self.cleaned_data.get(field):
            if not value:
                raise forms.ValidationError("This field is required")
        return value

    def get_institution(self, name):
        if not name:
            return None
        try:
            institution = models.Institution.objects.get(name=name)
        except models.Institution.DoesNotExist:
            institution = None
        except models.Institution.MultipleObjectsReturned:
            institution = models.Institution.objects.filter(name=name).first()
        return institution

    def clean_high_school_name(self):
        high_school_name = self.required_if_field_true('high_school_name', 'is_high_school_student')
        return high_school_name, self.get_institution(name=high_school_name)

    def clean_high_school_graduation_date(self):
        return self.required_if_field_true("high_school_graduation_date", "is_high_school_student")

    def clean_college_1_name(self):
        college_name = self.cleaned_data.get('college_1_name', '')
        return college_name, self.get_institution(name=college_name)

    def clean_college_2_name(self):
        college_name = self.required_if_field_true("college_2_name", "attends_college_2")
        return college_name, self.get_institution(name=college_name)

    def clean_college_3_name(self):
        college_name = self.required_if_field_true("college_3_name", "attends_college_3")
        return college_name, self.get_institution(name=college_name)

    def clean_college_4_name(self):
        college_name = self.required_if_field_true("college_4_name", "attends_college_4")
        return college_name, self.get_institution(name=college_name)

    def clean_is_high_school_student(self):
        return int(self.cleaned_data.get('is_high_school_student')) == 1

    def clean_college_1_degree(self):
        degree = self.cleaned_data.get('college_1_degree')
        if degree:
            return models.RefDegree.objects.get(code=degree)
        else:
            return None

    def clean_college_2_degree(self):
        degree = self.cleaned_data.get('college_2_degree')
        if degree:
            return models.RefDegree.objects.get(code=degree)
        else:
            return None

    def clean_college_3_degree(self):
        degree = self.cleaned_data.get('college_3_degree')
        if degree:
            return models.RefDegree.objects.get(code=degree)
        else:
            return None

    def clean_college_4_degree(self):
        degree = self.cleaned_data.get('college_4_degree')
        if degree:
            return models.RefDegree.objects.get(code=degree)
        else:
            return None

    def get_major(self, major_name):
        if not major_name:
            return None
        try:
            major = models.RefMajor.objects.get(name=major_name)
        except models.RefMajor.DoesNotExist:
            major = None
        return major

    def clean_college_1_major(self):
        major_name = self.cleaned_data.get('college_1_major', '')
        return major_name, self.get_major(major_name=major_name)

    def clean_college_2_major(self):
        major_name = self.cleaned_data.get('college_2_major', '')
        return major_name, self.get_major(major_name=major_name)

    def clean_college_3_major(self):
        major_name = self.cleaned_data.get('college_3_major', '')
        return major_name, self.get_major(major_name=major_name)

    def clean_college_4_major(self):
        major_name = self.cleaned_data.get('college_4_major', '')
        return major_name, self.get_major(major_name=major_name)

    def save(self, user_id, commit=True):
        user = User.objects.get(pk=user_id)

        try:
            user_profile = models.UserProfile.objects.get(user=user)
        except models.UserProfile.DoesNotExist:
            user_profile = models.UserProfile(user=user)

        profile_picture = self.cleaned_data.get('profile_picture')
        if profile_picture:
            user_profile.profile_picture = profile_picture

        cover_image = self.cleaned_data.get('cover_image')
        if cover_image:
            user_profile.cover_image = cover_image

        if commit:
            user_profile.save()

        # Updating student
        try:
            student = models.Student.objects.get(user_profile=user_profile)
        except models.Student.DoesNotExist:
            student = models.Student(user_profile=user_profile)

        student.is_high_school_student = self.cleaned_data.get('is_high_school_student')
        student.grad_story = self.cleaned_data.get('grad_story')
        student.overall_gpa = self.cleaned_data.get('overall_gpa')
        student.hobbies = self.cleaned_data.get('hobbies')
        student.volunteer_projects_internships = self.cleaned_data.get('volunteer_projects_internships')
        student.more_about_me = self.cleaned_data.get('more_about_me')

        if commit:
            student.save()

        # Updating student high school
        try:
            student_high_school = student.high_schools.get(order=1)
        except models.StudentHighSchool.DoesNotExist:
            student_high_school = models.StudentHighSchool(order=1, student=student)

        student_high_school.name, student_high_school.institution = self.cleaned_data.get('high_school_name')
        student_high_school.end_date = self.cleaned_data.get('high_school_graduation_date')
        student_high_school.projects = self.cleaned_data.get('high_school_projects')
        student_high_school.story = self.cleaned_data.get('high_school_story')

        if commit:
            student_high_school.save()

        # Updating student colleges
        self.save_student_colleges(commit=commit, student=student)

        student.sort_colleges()

    def save_student_colleges(self, commit, student):
        data = self.cleaned_data

        try:
            student_college_1 = student.colleges.get(order=1)
        except models.StudentCollege.DoesNotExist:
            student_college_1 = models.StudentCollege(order=1, student=student)

        student_college_1.name, student_college_1.institution = data.get('college_1_name')
        student_college_1.start_date = data.get('college_1_start_date')
        student_college_1.end_date = data.get('college_1_end_date')
        student_college_1.degree = data.get('college_1_degree')
        student_college_1.major_name, student_college_1.major = data.get('college_1_major')
        student_college_1.subjects = data.get('college_1_subjects')
        student_college_1.projects = data.get('college_1_projects')
        student_college_1.story = data.get('college_1_story')

        if commit:
            student_college_1.save()

        if data.get('attends_college_2'):

            try:
                student_college_2 = student.colleges.get(order=2)
            except models.StudentCollege.DoesNotExist:
                student_college_2 = models.StudentCollege(order=2, student=student)

            student_college_2.name, student_college_2.institution = data.get('college_2_name')
            student_college_2.start_date = data.get('college_2_start_date')
            student_college_2.end_date = data.get('college_2_end_date')
            student_college_2.degree = data.get('college_2_degree')
            student_college_2.major_name, student_college_2.major = data.get('college_2_major')
            student_college_2.subjects = data.get('college_2_subjects')
            student_college_2.projects = data.get('college_2_projects')
            student_college_2.story = data.get('college_2_story')

            if commit:
                student_college_2.save()

        else:

            if commit:
                try:
                    student_college_2 = student.colleges.get(order=2)
                    student_college_2.delete()
                except models.StudentCollege.DoesNotExist:
                    pass

        if data.get('attends_college_3'):

            try:
                student_college_3 = student.colleges.get(order=3)
            except models.StudentCollege.DoesNotExist:
                student_college_3 = models.StudentCollege(order=3, student=student)

            student_college_3.name, student_college_3.institution = data.get('college_3_name')
            student_college_3.start_date = data.get('college_3_start_date')
            student_college_3.end_date = data.get('college_3_end_date')
            student_college_3.degree = data.get('college_3_degree')
            student_college_3.major_name, student_college_3.major = data.get('college_3_major')
            student_college_3.subjects = data.get('college_3_subjects')
            student_college_3.projects = data.get('college_3_projects')
            student_college_3.story = data.get('college_3_story')

            if commit:
                student_college_3.save()
        else:

            if commit:
                try:
                    student_college_3 = student.colleges.get(order=3)
                    student_college_3.delete()
                except models.StudentCollege.DoesNotExist:
                    pass

        if data.get('attends_college_4'):

            try:
                student_college_4 = student.colleges.get(order=4)
            except models.StudentCollege.DoesNotExist:
                student_college_4 = models.StudentCollege(order=4, student=student)

            student_college_4.name, student_college_4.institution = data.get('college_4_name')
            student_college_4.start_date = data.get('college_4_start_date')
            student_college_4.end_date = data.get('college_4_end_date')
            student_college_4.degree = data.get('college_4_degree')
            student_college_4.major_name, student_college_4.major = data.get('college_4_major')
            student_college_4.subjects = data.get('college_4_subjects')
            student_college_4.projects = data.get('college_4_projects')
            student_college_4.story = data.get('college_4_story')

            if commit:
                student_college_4.save()

        else:

            if commit:
                try:
                    student_college_4 = student.colleges.get(order=4)
                    student_college_4.delete()
                except models.StudentCollege.DoesNotExist:
                    pass


class StudentAccount529Form(forms.ModelForm):
    parent_phone_number = forms.CharField(widget=forms.TextInput(attrs={'required': 'required',
                                                                        'class': 'us-phone-mask'}), required=True)

    class Meta:
        model = models.StudentAccount529
        fields = '__all__'
        exclude = ('student',)


class ContactForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'required': 'required'}), required=True)
    email = forms.EmailField(widget=forms.EmailInput(attrs={'required': 'required'}), required=True)
    subject = forms.CharField(widget=forms.TextInput(attrs={'required': 'required'}), required=True)

    class Meta:
        model = models.Contact
        fields = '__all__'
        exclude = ('user',)