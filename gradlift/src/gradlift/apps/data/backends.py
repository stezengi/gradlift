from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email


class BasicBackend:
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id, is_active=True)
        except User.DoesNotExist:
            raise ValidationError("Please enter a correct email and password.")


class EmailBackend(BasicBackend):
    def authenticate(self, username=None, password=None):
        #If username is an email address, then try to pull it up

        try:
            validate_email(username)
        except ValidationError:
            try:
                user = User.objects.get(username=username, is_active=True)
            except User.DoesNotExist:
                raise ValidationError("Please enter a correct email and password.")
        else:
            try:
                user = User.objects.get(email=username, is_active=True)
            except User.DoesNotExist:
                raise ValidationError("Please enter a correct email and password.")

        if user.check_password(password):
            return user