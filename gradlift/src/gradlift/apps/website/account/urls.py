from django.conf.urls import url
from gradlift.apps.website.account import views

urlpatterns = [
    url(r'^$', views.AccountView.as_view(), name='account'),
    url(r'^general/$', views.AccountGeneralView.as_view(), name='account-general'),
    url(r'^password-reset/$', views.AccountPasswordChangeView.as_view(), name='account-password-reset'),
    url(r'^funds-received/$', views.AccountFundsReceivedView.as_view(), name='account-funds-received'),
    url(r'^backed-students/$', views.AccountBackedStudentsView.as_view(), name='account-backed-students'),
    url(r'^disable-account/$', views.DisableAccountView.as_view(), name='account-disable-account'),
]