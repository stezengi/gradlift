
from django.views.generic import View, FormView
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import render, redirect

from gradlift.apps import Constants
from gradlift.apps.data.account import forms
from gradlift.apps.data import models
from gradlift.apps.website.views import LoginRequiredView


class AccountBaseView(LoginRequiredView):

    def get_context_data(self, **kwargs):
        if hasattr(super(AccountBaseView, self), 'get_context_data'):
            context = super(AccountBaseView, self).get_context_data(**kwargs)
        else:
            context = {}

        context['user_id'] = self.request.user.pk
        context['profile_header'] = self.request.user.profile.get_profile_overview_dict()
        context['pledge_info'] = self.request.user.profile.student.get_pledge_view_info()
        context['is_logged_in_user'] = True
        context['image_upload_modals'] = self.request.user.profile.get_image_upload_modal_dics()

        return context


class AccountView(LoginRequiredView, View):

    def get(self, request):
        return redirect('account-general')


class AccountGeneralView(AccountBaseView, FormView):
    form_class = forms.AccountGeneralForm
    template_name = "account/general.html"

    def form_invalid(self, form):
        messages.error(self.request, "Error! Your Account was not saved successfully. Please scroll down for more details.")
        return super(AccountGeneralView, self).form_invalid(form)

    def form_valid(self, form):
        form.save(user_id=self.request.user.pk)
        messages.add_message(self.request, messages.SUCCESS, "Your account changes are saved successfully!")
        return super(AccountGeneralView, self).form_valid(form)

    def get_success_url(self):
        return reverse('account-general')

    def get_context_data(self, **kwargs):
        context = super(AccountGeneralView, self).get_context_data(**kwargs)
        return context

    def get_form_kwargs(self):
        kw = super(AccountGeneralView, self).get_form_kwargs()
        kw['request'] = self.request # the trick!
        return kw

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(AccountGeneralView, self).get_initial()
        profile = self.request.user.profile

        if profile.referral_code:
            initial['referral_code'] = profile.referral_code.code

        if profile.birthday:
            initial['birthday'] = profile.birthday.strftime(Constants.MONTH_DAY_YEAR_NORMAL_FORMAT)

        initial['zip_code'] = profile.zip_code
        initial['city'] = profile.city
        initial['street'] = profile.street

        if profile.state:
            initial['state'] = profile.state.code

        if profile.country:
            initial['country'] = profile.country.code
        else:
            initial['country'] = models.RefCountry.objects.get_usa().code

        initial['notification_frequency'] = profile.notification_frequency

        initial['email'] = self.request.user.email
        initial['first_name'] = self.request.user.first_name
        initial['last_name'] = self.request.user.last_name

        return initial


class AccountPasswordChangeView(AccountBaseView, FormView):
    form_class = forms.AccountPasswordChangeForm
    template_name = "account/password-change.html"

    def get(self, request, *args, **kwargs):

        try:
            if not request.user.is_anonymous():
                request.user.profile
        except (models.UserProfile.DoesNotExist, AttributeError,):
            return redirect('logout')

        context_data = super(AccountPasswordChangeView, self).get_context_data()
        context_data['form'] = self.form_class(self.get_form_kwargs())

        return render(request, self.template_name, context_data)

    def form_invalid(self, form):
        messages.error(self.request, "Error! Your password was not saved successfully. Please scroll down for more details.")
        return super(AccountPasswordChangeView, self).form_invalid(form)

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        messages.add_message(self.request, messages.SUCCESS, "Your password was changed successfully!")
        return super(AccountPasswordChangeView, self).form_valid(form)

    def get_success_url(self):
        return reverse('account-general')

    def get_form_kwargs(self):
        kw = super(AccountPasswordChangeView, self).get_form_kwargs()
        kw['user'] = self.request.user
        kw['data'] = self.request.POST
        return kw

    def get_context_data(self, **kwargs):
        context = super(AccountPasswordChangeView, self).get_context_data(**kwargs)
        return context


class AccountFundsReceivedView(AccountBaseView):
    template_name = "account/funds-received.html"

    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        context_data = self.get_context_data()
        context_data['funds_received'] = self.request.user.profile.student.donations_received.get_dicts()
        return render(request, self.template_name, context_data)


class AccountBackedStudentsView(AccountBaseView):
    template_name = "account/backed-students.html"

    http_method_names = ['get']

    def get(self, request, *args, **kwargs):

        try:
            backer = self.request.user.profile.backer
        except models.Backer.DoesNotExist:
            backer = None

        context_data = self.get_context_data()
        context_data['backer_pledge_info'] = backer.get_pledge_view_info() if backer else None
        context_data['backed_students'] = backer.contributions.get_dicts() if backer else None

        return render(request, self.template_name, context_data)


class DisableAccountView(AccountBaseView, FormView):
    form_class = forms.DisableAccountForm
    template_name = "account/disable-account.html"

    def form_invalid(self, form):
        messages.error(self.request, "Error! Your account was not disabled successfully. Please scroll down for more details.")
        return super(DisableAccountView, self).form_invalid(form)

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, self.request.user)
        messages.add_message(self.request, messages.SUCCESS, "Your account was disabled successfully!")
        return super(DisableAccountView, self).form_valid(form)

    def get_success_url(self):
        return reverse('logout')

    def get_form_kwargs(self):
        kw = super(DisableAccountView, self).get_form_kwargs()
        kw['request'] = self.request
        return kw

    def get_context_data(self, **kwargs):
        context = super(DisableAccountView, self).get_context_data(**kwargs)
        return context