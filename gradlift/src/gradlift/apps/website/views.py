import logging
import json
import requests
import tempfile

from django.shortcuts import render
from django.views.generic import View, FormView, RedirectView
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.files import File
from django.contrib.auth.views import password_reset_confirm
from django.shortcuts import redirect

from braces.views import LoginRequiredMixin

from social.apps.django_app.default.models import UserSocialAuth

from gradlift.apps import Constants
from gradlift.apps.data import forms
from gradlift.apps.data import models


logger = logging.getLogger(__name__)


class IndexView(View):
    template_name = "index/index.html"

    def get(self, request, *args, **kwargs):

        try:
            if not request.user.is_anonymous():
                request.user.profile
        except (models.UserProfile.DoesNotExist, AttributeError,):
            return redirect('logout')

        featured_profile_overviews, featured_profile_overviews_max_page_number = models.UserProfile.objects.get_featured_profile_overviews()
        recently_added_profile_overviews, recently_added_profile_overviews_max_page_number = models.UserProfile.objects.get_recently_added_profile_overviews()
        high_school_profile_overviews, high_school_profile_overviews_max_page_number = models.UserProfile.objects.get_high_school_profile_overviews()
        college_profile_overviews, college_profile_overviews_max_page_number = models.UserProfile.objects.get_college_profile_overviews()
        grad_school_profile_overviews, grad_school_profile_overviews_max_page_number = models.UserProfile.objects.get_grad_school_profile_overviews()

        return render(request, self.template_name, {
            'featured_profile_overviews': featured_profile_overviews,
            'featured_profile_overviews_max_page_number': featured_profile_overviews_max_page_number,
            'recently_added_profile_overviews': recently_added_profile_overviews,
            'recently_added_profile_overviews_max_page_number': recently_added_profile_overviews_max_page_number,
            'high_school_profile_overviews': high_school_profile_overviews,
            'high_school_profile_overviews_max_page_number': high_school_profile_overviews_max_page_number,
            'college_profile_overviews': college_profile_overviews,
            'college_profile_overviews_max_page_number': college_profile_overviews_max_page_number,
            'grad_school_profile_overviews': grad_school_profile_overviews,
            'grad_school_profile_overviews_max_page_number': grad_school_profile_overviews_max_page_number,
            'blogs': models.Blog.objects.get_most_recent_blogs(max_number=3),
            'is_blog_summary': True
        })


class LoginView(FormView):
    template_name = "registration/login.html"
    form_class = forms.LoginForm
    user = None

    def form_invalid(self, form):
        return super(LoginView, self).form_invalid(form)

    def form_valid(self, form):
        self.user = form.user
        login(self.request, self.user)
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        profile = models.UserProfile.objects.get(user=self.user)
        return reverse('profile', kwargs={'profile_link': profile.link})


class PostAuthenticated(View):
    def get(self, request, *args, **kwargs):

        is_first_login = False

        try:
            user_profile = models.UserProfile.objects.get(user=self.request.user)
        except models.UserProfile.DoesNotExist:
            user_profile = models.UserProfile(user=self.request.user)
            user_profile.save()
            is_first_login = True

        try:
            user_profile.student
        except models.Student.DoesNotExist:
            student = models.Student(user_profile=user_profile)
            student.save()
            student.send_welcome_email()
            is_first_login = True

        if is_first_login:
            duplicate_users = models.User.objects.filter(email=self.request.user.email).exclude(pk=self.request.user.pk)
            if duplicate_users.count() > 0:

                provider_code_to_name = {
                    'facebook': 'Facebook',
                    'google-oauth2': 'Google'
                }

                for duplicate_user in duplicate_users:
                    try:
                        social_auth = duplicate_user.social_auth.get()
                    except UserSocialAuth.DoesNotExist:
                        message = "Error! You have already registered using your email %s." % self.request.user.email
                    else:
                        account = provider_code_to_name[social_auth.provider]
                        message = "Error! You have already registered using your %s account with email %s." % (
                            account, self.request.user.email,)

                user_to_delete = self.request.user
                logout(request)

                user_to_delete.profile.student.delete()
                user_to_delete.profile.delete()
                user_to_delete.social_auth.all().delete()
                user_to_delete.delete()

                messages.error(self.request, message)
                return redirect('login')

        try:
            if is_first_login:

                profile_image_file = None
                cover_image_file = None

                try:
                    profile_image_file, cover_image_file = self.get_facebook_profile_and_cover_image(request)
                except UserSocialAuth.DoesNotExist:
                    pass

                try:
                    profile_image_file, cover_image_file = self.get_google_profile_and_cover_image(request)
                except UserSocialAuth.DoesNotExist:
                    pass

                if profile_image_file:
                    request.user.profile.profile_picture.save(
                        request.user.profile.link+".profile.picture.jpg",
                        profile_image_file)

                if cover_image_file:
                    request.user.profile.cover_image.save(
                        request.user.profile.link+".cover.image.jpg",
                        cover_image_file)

                request.user.profile.save()

        except(KeyError, IndexError, AttributeError, TypeError) as e:
            logger.warning(e.message)

        if is_first_login:
            messages.add_message(self.request, messages.SUCCESS, "You are registered!")
            return "%s?%s" % (reverse('profile-edit'), Constants.ANALYTICS_PARAM_REGISTER,)
        else:
            return redirect('profile', self.request.user.profile.link)

    def get_google_profile_and_cover_image(self, request):
        social_auth = request.user.social_auth.get(provider="google-oauth2")
        profile_image_url = "https://www.googleapis.com/oauth2/v1/userinfo?access_token={access_token}".format(
            access_token=social_auth.extra_data['access_token']
        )
        response = requests.get(url=profile_image_url)
        profile_image_json = json.loads(response.content)
        profile_image_source = profile_image_json['picture']
        profile_image_file = self.get_remote_image(image_url=profile_image_source)
        return profile_image_file, None

    def get_facebook_profile_and_cover_image(self, request):
        social_auth = request.user.social_auth.get(provider="facebook")
        profile_image_url = "http://graph.facebook.com/{uid}/picture?width=200&height=200".format(
            uid=social_auth.uid)

        profile_image_file = self.get_remote_image(image_url=profile_image_url)

        cover_image_url = "https://graph.facebook.com/{uid}?fields=cover&access_token={access_token}".format(
            uid=social_auth.uid,
            access_token=social_auth.extra_data['access_token']
        )

        response = requests.get(url=cover_image_url)
        cover_image_json = json.loads(response.content)

        try:
            cover_image_source = cover_image_json['cover']['source']
            cover_image_file = self.get_remote_image(image_url=cover_image_source)
        except KeyError:
            cover_image_file = None

        return profile_image_file, cover_image_file

    def get_remote_image(self, image_url):
        # Steam the image from the url
        request = requests.get(image_url, stream=True)

        # Was the request OK?
        if request.status_code != requests.codes.ok:
            # Nope, error handling, skip file etc etc etc
            logger.info("Image %s was not retrieved successfully" % image_url)

        # Create a temporary file
        lf = tempfile.NamedTemporaryFile()

        # Read the streamed image in sections
        for block in request.iter_content(1024 * 8):

            # If no more file then stop
            if not block:
                break

            # Write image block to temporary file
            lf.write(block)

        return File(lf)


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        logout(request)
        url = reverse('index')
        return HttpResponseRedirect(url)


class RegisterView(FormView):
    template_name = "registration/register.html"
    form_class = forms.RegistrationForm
    user = None

    def form_invalid(self, form):
        return super(RegisterView, self).form_invalid(form)

    def form_valid(self, form):
        self.user = form.save()
        user = authenticate(username=form.cleaned_data.get('email'), password=form.cleaned_data.get('password1'))
        login(self.request, user)
        return super(RegisterView, self).form_valid(form)

    def get_success_url(self):
        return "%s?%s" % (reverse('profile-edit'), Constants.ANALYTICS_PARAM_REGISTER,)


class PasswordResetView(FormView):
    template_name = "registration/password-reset.html"
    form_class = forms.PasswordResetForm

    def form_invalid(self, form):

        return super(PasswordResetView, self).form_invalid(form)

    def form_valid(self, form):

        form.save(from_email=settings.DEFAULT_FROM_EMAIL, domain_override=settings.BASE_URL)

        return super(PasswordResetView, self).form_valid(form)

    def get_success_url(self):
        return reverse('password-reset-email-sent')


class PasswordResetEmailSentView(View):
    template_name = "registration/password-reset-email-sent.html"

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {})


class PasswordResetConfirmView(View):
    form_class = forms.SetPasswordForm
    template_name = "registration/password-reset-confirm.html"

    def get(self, request, uidb64=None, token=None):
        return self.process_request(request=request, uidb64=uidb64, token=token)

    def post(self, request,  uidb64=None, token=None):
        return self.process_request(request=request, uidb64=uidb64, token=token)

    def process_request(self, request,  uidb64=None, token=None):
        return password_reset_confirm(request, template_name='registration/password-reset-confirm.html',
                                      uidb64=uidb64, token=token, post_reset_redirect=reverse('login'),
                                      set_password_form=forms.SetPasswordForm)


class AboutGradliftView(View):
    template_name = "about-gradlift.html"

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {})


class TermsOfServiceView(View):
    template_name = "terms-of-service.html"

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {})


class PrivacyPolicyView(View):
    template_name = "privacy-policy.html"

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {})


class FAQView(View):
    template_name = "faq.html"

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name, {})


class BlogView(View):
    template_name = "blog/blog.html"

    def get(self, request, blog_link):
        blog = models.Blog.objects.get_blog_by_link(blog_link=blog_link)
        return render(request, self.template_name, {'blog': blog, 'is_blog_summary': False})


class BlogsView(View):
    template_name = "blog/blogs.html"

    def get(self, request):
        blogs = models.Blog.objects.get_most_recent_blogs()
        return render(request, self.template_name, {'blogs': blogs, 'is_blog_summary': True})


class AboutUsView(View):
    template_name = "about-us.html"

    def get(self, request):
        return render(request, self.template_name, {})


class ProfileView(View):
    template_name = "profile/profile-view.html"

    def get(self, request, profile_link):

        try:
            profile = models.UserProfile.objects.get(link=profile_link)
            user = profile.user
        except models.UserProfile.DoesNotExist:
            # If the given user id does not exist redirect to the logged in user instead and log it as a warning
            logger.warning("User with profile_link (%s) does not exist. (called in profile view)", profile_link)
            return redirect('profile', self.request.user.profile.link)

        user_id = user.pk
        is_logged_in_user = request.user and request.user.pk and request.user.pk == int(user_id)

        if request.user:
            logged_in_user_id = request.user.pk
        else:
            logged_in_user_id = None

        return render(request, self.template_name, {
            'user_id': user_id,
            'is_logged_in_user': is_logged_in_user,
            'profile_header': profile.get_profile_overview_dict(),
            'profile': profile.get_profile_view_dict(),
            'pledge_info': profile.student.get_pledge_view_info(),
            'image_upload_modals': profile.get_image_upload_modal_dics(),
            'activities': profile.get_comments(logged_in_user_id=logged_in_user_id)
        })


class ProfileCompletedView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        profile = models.UserProfile.objects.get(user=self.request.user)
        return reverse('profile', kwargs={'profile_link': profile.link})


class LoginRequiredView(LoginRequiredMixin, View):
    login_url = "/login/"


class ActivitySaveView(LoginRequiredView, FormView):

    def post(self, request):
        form = forms.ActivityForm(request.POST)

        if form.is_valid():
            form.save(request)

        user_id = form.cleaned_data.get('user_id')
        user = models.User.objects.get(pk=user_id)
        messages.add_message(self.request, messages.SUCCESS, "You have added a new comment")
        return redirect('profile', user.profile.link)


class CommentDeleteView(LoginRequiredView, FormView):

    def post(self, request):

        form = forms.CommentDeleteForm(request.POST)

        if form.is_valid():
            comment = models.Comment.objects.get(pk=form.cleaned_data.get('comment_id'))

            user_id = form.cleaned_data.get('commentee_user_id')
            user = models.User.objects.get(pk=user_id)

            if request.user.pk == comment.commenter.user.pk:
                comment.is_removed = True
                comment.save()
                messages.add_message(self.request, messages.WARNING, "You have removed your comment")

        return redirect('profile', user.profile.link)


class AgreedToTermsView(LoginRequiredView, FormView):

    def post(self, request):
        request.user.profile.has_agreed_to_terms = True
        request.user.profile.save()
        messages.add_message(self.request, messages.SUCCESS, "You have agreed to our terms")
        return redirect('gradlift-scholarship')


class ProfileEditView(LoginRequiredView, FormView):
    form_class = forms.UserProfileForm
    template_name = "profile/profile-edit.html"

    def form_invalid(self, form):
        messages.error(self.request, "Error! You profile was not saved successfully. Please scroll down for more details.")
        return super(ProfileEditView, self).form_invalid(form)

    def form_valid(self, form):
        profile_before_update = models.UserProfile.objects.get(user=self.request.user).is_profile_completed
        logger.info(profile_before_update)
        form.save(user_id=self.request.user.pk)

        profile_after_update = models.UserProfile.objects.get(user=self.request.user).is_profile_completed
        logger.info(profile_after_update)
        if not profile_before_update and profile_after_update:
            self.request.session['is_profile_completed_first_time'] = True
        else:

            self.request.session['is_profile_completed_first_time'] = False
        messages.add_message(self.request, messages.SUCCESS, "Your profile changes are saved successfully!")
        return super(ProfileEditView, self).form_valid(form)

    def get_success_url(self):
        profile = models.UserProfile.objects.get(user=self.request.user)

        if self.request.session['is_profile_completed_first_time']:
            return "%s?%s" % (reverse('profile-completed'),
                              Constants.ANALYTICS_PARAM_PROFILE_COMPLETED,)

        return reverse('profile', kwargs={'profile_link': profile.link})

    def get_context_data(self, **kwargs):
        context = super(ProfileEditView, self).get_context_data(**kwargs)
        context['institutions'] = [institution.encode("utf8") for institution in models.Institution.objects.values_list(
            'name', flat=True).order_by('name')]

        context['majors'] = [major.encode("utf8") for major in models.RefMajor.objects.values_list(
            'name', flat=True).order_by('name')]

        context['image_upload_modals'] = self.request.user.profile.get_image_upload_modal_dics()

        context['pledge_info'] = self.request.user.profile.student.get_pledge_view_info()

        context['profile_header'] = self.request.user.profile.get_profile_overview_dict()

        context['image_upload_modals'] = self.request.user.profile.get_image_upload_modal_dics()

        context['user_id'] = self.request.user.pk

        context['is_logged_in_user'] = True

        return context

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(ProfileEditView, self).get_initial()

        try:
            student = self.request.user.profile.student
        except (models.UserProfile.DoesNotExist, models.Student.DoesNotExist):
            initial['is_high_school_student'] = 0
            initial['attends_college_2'] = 0
            initial['attends_college_3'] = 0
            initial['attends_college_4'] = 0
            return initial

        initial['grad_story'] = student.grad_story
        initial['overall_gpa'] = student.overall_gpa
        initial['hobbies'] = student.hobbies

        initial['is_high_school_student'] = 1 if student.is_high_school_student else 0
        try:
            high_school = student.high_schools.get(order=1)
            initial['high_school_name'] = high_school.name

            if high_school.end_date:
                initial['high_school_graduation_date'] = high_school.end_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            initial['high_school_projects'] = high_school.projects
            initial['high_school_story'] = high_school.story
        except models.StudentHighSchool.DoesNotExist:
            pass

        try:
            college = student.colleges.get(order=1)
            initial['college_1_name'] = college.name

            if college.start_date:
                initial['college_1_start_date'] = college.start_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.end_date:
                initial['college_1_end_date'] = college.end_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.degree:
                initial['college_1_degree'] = college.degree.code

            initial['college_1_major'] = college.major_name
            initial['college_1_subjects'] = college.subjects
            initial['college_1_projects'] = college.projects
            initial['college_1_story'] = college.story

        except models.StudentCollege.DoesNotExist:
            pass

        try:
            college = student.colleges.get(order=2)
            initial['attends_college_2'] = 1
            initial['college_2_name'] = college.name

            if college.start_date:
                initial['college_2_start_date'] = college.start_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.end_date:
                initial['college_2_end_date'] = college.end_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.degree:
                initial['college_2_degree'] = college.degree.code

            initial['college_2_major'] = college.major_name
            initial['college_2_subjects'] = college.subjects
            initial['college_2_projects'] = college.projects
            initial['college_2_story'] = college.story

        except models.StudentCollege.DoesNotExist:
            initial['attends_college_2'] = 0

        try:
            college = student.colleges.get(order=3)
            initial['attends_college_3'] = 1
            initial['college_3_name'] = college.name

            if college.start_date:
                initial['college_3_start_date'] = college.start_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.end_date:
                initial['college_3_end_date'] = college.end_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.degree:
                initial['college_3_degree'] = college.degree.code

            initial['college_3_major'] = college.major_name
            initial['college_3_subjects'] = college.subjects
            initial['college_3_projects'] = college.projects
            initial['college_3_story'] = college.story

        except models.StudentCollege.DoesNotExist:
            initial['attends_college_3'] = 0

        try:
            college = student.colleges.get(order=4)
            initial['attends_college_4'] = 1
            initial['college_4_name'] = college.name

            if college.start_date:
                initial['college_4_start_date'] = college.start_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.end_date:
                initial['college_4_end_date'] = college.end_date.strftime(Constants.MONTH_YEAR_DATE_FORMAT)

            if college.degree:
                initial['college_4_degree'] = college.degree.code

            initial['college_4_major'] = college.major_name
            initial['college_4_subjects'] = college.subjects
            initial['college_4_projects'] = college.projects
            initial['college_4_story'] = college.story

        except models.StudentCollege.DoesNotExist:
            initial['attends_college_4'] = 0

        initial['volunteer_projects_internships'] = student.volunteer_projects_internships
        initial['more_about_me'] = student.more_about_me

        return initial


class GradliftScholarshipView(LoginRequiredView, FormView):
    form_class = forms.StudentAccount529Form
    template_name = "gradlift-scholarship.html"

    def get_form(self, form_class):
        """
        Check if the user already saved contact details. If so, then show
        the form populated with those details, to let user change them.
        """
        try:
            student_account = models.StudentAccount529.objects.get(student=self.request.user.profile.student)
            return form_class(instance=student_account, **self.get_form_kwargs())
        except models.StudentAccount529.DoesNotExist:
            return form_class(**self.get_form_kwargs())

    def form_invalid(self, form):
        messages.error(self.request, "Error! Account information was not saved properly. Please scroll down for more details.")
        return super(GradliftScholarshipView, self).form_invalid(form)

    def form_valid(self, form):
        form.instance.student = self.request.user.profile.student
        form.save()
        messages.add_message(self.request, messages.SUCCESS, "Account information was saved successfully")
        return super(GradliftScholarshipView, self).form_valid(form)

    def get_success_url(self):
        return reverse('gradlift-scholarship')

    def get_context_data(self, **kwargs):
        context = super(GradliftScholarshipView, self).get_context_data(**kwargs)

        context['profile_header'] = self.request.user.profile.get_profile_overview_dict()
        context['is_logged_in_user'] = True
        context['is_gradline_page'] = True
        context['loan_documents'] = self.request.user.profile.student.get_all_loan_documents()
        context['is_profile_completed'] = self.request.user.profile.is_profile_completed
        context['has_agreed_to_terms'] = self.request.user.profile.has_agreed_to_terms
        context['has_uploaded_loan_documents'] = self.request.user.profile.student.has_uploaded_loan_documents
        context['are_loan_documents_verified'] = self.request.user.profile.student.are_loan_documents_verified
        context['user_id'] = self.request.user.pk
        context['goal_amount'] = self.request.user.profile.student.goal_amount

        return context


class DiscoverView(View):
    template_name = "discover.html"

    def get(self, request, *args, **kwargs):

        search_term = request.GET.get('search-term', None)

        if search_term is None or len(str(search_term).strip()) == 0:
            profile_overviews, profile_overviews_max_page_number = models.UserProfile.objects.get_all_profile_overviews()
        else:
            profile_overviews, profile_overviews_max_page_number = models.UserProfile.objects.discover_profile_overviews(
                search_term=search_term)

        return render(request, self.template_name, {
            'profile_overviews': profile_overviews,
            'profile_overviews_max_page_number': profile_overviews_max_page_number,
            'search_term': search_term
        })


class ContactView(FormView):
    form_class = forms.ContactForm
    template_name = "contact.html"

    def form_invalid(self, form):
        messages.error(self.request, "Error! Your message was not sent properly. Please complete all the fields properly if you want your email to reach it's destination!")
        return super(ContactView, self).form_invalid(form)

    def form_valid(self, form):
        if not self.request.user.is_anonymous():
            form.instance.user_profile = self.request.user.profile
        form.save()
        messages.add_message(self.request, messages.SUCCESS, "Your message was received! We will look in to your inquiry as quickly as possible and then get back to you.")
        return super(ContactView, self).form_valid(form)

    def get_success_url(self):
        return reverse('index')

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        return context