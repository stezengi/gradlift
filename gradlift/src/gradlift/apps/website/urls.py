from django.conf.urls import include, url

from django.views.decorators.cache import cache_page

from django.conf import settings
from django.conf.urls.static import static

from gradlift.apps.website import views
from gradlift.apps import Constants


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^find-students/$', views.DiscoverView.as_view(), name='discover'),
    url(r'^login/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='login-page')(views.LoginView.as_view()),
        name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^register/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='register-page')(views.RegisterView.as_view()),
        name='register'),
    url(r'^password-reset/$', views.PasswordResetView.as_view(), name='password-reset'),
    url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', views.PasswordResetConfirmView.as_view(), name='password-reset-confirm'),
    url(r'^password-reset-email-sent/$', views.PasswordResetEmailSentView.as_view(), name='password-reset-email-sent'),
    url(r'^post-authenticated/$', views.PostAuthenticated.as_view(), name='post-authenticated'),
    url(r'^contact/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='contact-page')(views.ContactView.as_view()),
        name='contact'),
    url(r'^terms-of-service/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='terms-of-service-page')(views.TermsOfServiceView.as_view()),
        name='terms-of-service'),
    url(r'^privacy-policy/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='privacy-policy-page')(views.PrivacyPolicyView.as_view()),
        name='privacy-policy'),
    url(r'^faq/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='faq-page')(views.FAQView.as_view()),
        name='faq'),
    url(r'^blogs/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='blogs-page')(views.BlogsView.as_view()),
        name='blogs'),
    url(r'^about-us/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='about-us-page')(views.AboutUsView.as_view()),
        name='about-us'),
    url(r'^blog/(?P<blog_link>.+)/$',
        cache_page(Constants.CACHE_TIME_ONE_HOUR, key_prefix='blog-page')(views.BlogView.as_view()),
        name='blog'),
    url(r'^profile-completed/$', views.ProfileCompletedView.as_view(), name='profile-completed'),
    url(r'^profile/(?P<profile_link>.+)/$', views.ProfileView.as_view(), name='profile'),
    url(r'^gradlift-scholarship/$', views.GradliftScholarshipView.as_view(), name='gradlift-scholarship'),
    url(r'^profile-edit/$', views.ProfileEditView.as_view(), name='profile-edit'),
    url(r'^activity-save/$', views.ActivitySaveView.as_view(), name='activity-save'),
    url(r'^activity-delete/$', views.CommentDeleteView.as_view(), name='activity-delete'),
    url(r'^agreed-to-terms/$', views.AgreedToTermsView.as_view(), name='agreed-to-terms'),
    url(r'^account/', include('gradlift.apps.website.account.urls')),
    url(r'^ajax/', include('gradlift.apps.website.ajax.urls')),
] + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)
