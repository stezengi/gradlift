$(function(){

    $('#upload-profile-picture-button, #upload-cover-image-button').click(function(e) {
        $('.croppie-container').hide();
        $('.upload-result').hide();
        $('.file-drag-upload').show();
        $('.image-upload-back-bottom').hide();
    });

    $('.croppie-container').hide();
    $('.upload-result').hide();
    $('.file-drag-upload').show();

    var ul = $('.file-drag-upload ul');

    $('.image-upload-back-bottom').click(function(event) {
        $('.croppie-container').hide();
        $('.upload-result').hide();
        $('.image-upload-back-bottom').hide();
        $('.image-upload-close-bottom').show();
        $('.file-drag-upload').show();
    });

    $('.file-drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('.file-drag-upload').each(function(index, fileDragUploadElement) {

        var isProfileImage = $(fileDragUploadElement).attr('data-upload-type') == 'profile-image-upload';

        $(fileDragUploadElement).fileupload({

            // This element will accept file drag/drop uploading
            dropZone: $(fileDragUploadElement).find('.file-drop'),

            // This function is called when a file is added to the queue;
            // either via the browse button, or via drag/drop:
            add: function (e, data) {

                if (isProfileImage) {
                    $(fileDragUploadElement).hide();
                    $('.croppie-container').show();
                    $('.upload-result').show();
                    $('.image-upload-close-bottom').hide();
                    $('.image-upload-back-bottom').show();
                    readFile(data);
                    return;
                }

                var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

                // Append the file name and file size
                tpl.find('p').text(data.files[0].name)
                             .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

                // Add the HTML to the UL element
                data.context = tpl.appendTo(ul);

                // Initialize the knob plugin
                tpl.find('input').knob();

                // Listen for clicks on the cancel icon
                tpl.find('span').click(function(){

                    if(tpl.hasClass('working')){
                        jqXHR.abort();
                    }

                    tpl.fadeOut(function(){
                        tpl.remove();
                    });

                });

                // Automatically upload the file once it is added to the queue
                var jqXHR = data.submit();
            },

            progress: function(e, data){

                // Calculate the completion percentage of the upload
                var progress = parseInt(data.loaded / data.total * 100, 10);

                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();

                if(progress == 100){
                    data.context.removeClass('working');
                }
            },

            fail:function(e, data){
                // Something has gone wrong!
                data.context.addClass('error');
            },

            success: function(data) {
                $('#loan-statements-completed p')
                    .removeClass('incomplete')
                    .addClass('completed');
                $('#loan-statements-completed p span.loan-statements-completed-icon')
                    .removeClass('glyphicon-remove-circle')
                    .addClass('glyphicon-ok-circle');

                if (isProfileImage) {
                    $('#reviewing-docs-message').show();
                    $('#reviewed-docs-message').hide();
                    $('#loan-statements-completed-verified').hide();
                    $('#loan-statements-completed-submitted').show();
                }
            }

        });
    });

    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }


    var $uploadCrops = [];

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                for (var index in $uploadCrops) {
                    $uploadCrops[index].croppie('bind', {
                        url: e.target.result
                    });
                }

                $('.upload-demo').addClass('ready');
            };

            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $('.croppie-container').each(function(index, container) {

        var container_type = $(container).attr('data-file-input-name');

        var viewport = {
            width: 200,
            height: 200,
            type: 'rectangle'
        };

        var boundary = {
            width: 300,
            height: 300
        };

        if (container_type == 'profile_picture') {
            viewport = {
                width: 200,
                height: 200,
                type: 'rectangle'
            };

            boundary = {
                width: 300,
                height: 300
            };
        } else if (container_type == 'cover_image') {
            viewport = {
                width: 450,
                height: 160,
                type: 'rectangle'
            };

            boundary = {
                width: 500,
                height: 200
            };
        }

        var $uploadCrop = $(container).croppie({
            viewport: viewport,
            boundary: boundary,
            exif: true
        });

        $uploadCrops[index] = $uploadCrop;

    });

    $('.upload-result').on('click', function (ev) {
        var url = "/ajax/" + $(this).attr('data-action-url');

        var success_function_name = $(this).attr('data-onsuccess');
        var success_function = eval(success_function_name);

        var error_function_name = $(this).attr('data-onerror');
        var error_function = eval(error_function_name);

        var data_file_input_name = $(this).attr('data-file-input-name');

        for (var index in $uploadCrops) {
            $uploadCrops[index].croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {

                var blobBin = atob(resp.split(',')[1]);

                if (!blobBin) {
                    return;
                }

                var array = [];
                for(var i = 0; i < blobBin.length; i++) {
                    array.push(blobBin.charCodeAt(i));
                }
                var file=new Blob([new Uint8Array(array)], {type: 'image/png'});

                var formdata = new FormData();
                formdata.append(data_file_input_name, file);

                var ajaxData = {
                   url: url,
                   type: "POST",
                   data: formdata,
                   processData: false,
                   contentType: false,
                   success: success_function,
                   error: error_function
                };

                $.ajax(ajaxData);
            });
        }

    });

});