var setup_blogs_layout = function() {

    var browserWidth = $(window).width();

    var blogs = [];
    $(".all-blogs .blog-div").each(function(index, blog){
        blogs.push(blog);
    });

    $(".all-blogs > .row").remove();

    var numberOfColumns = 1;
    if (browserWidth >= 980) {
        numberOfColumns = 3;
    } else if (browserWidth >= 780) {
        numberOfColumns = 2;
    }

    var row = document.createElement('div');
    $(row).addClass('row');
    $(row).appendTo($(".all-blogs"));

    var columns = [];
    for (var i=0; i<numberOfColumns; i++) {
        var column = document.createElement('div');
        if (numberOfColumns == 3) {
            $(column).addClass('col-sm-4');
        } else if (numberOfColumns == 2) {
            $(column).addClass('col-sm-6');
        }
        columns.push(column);
        $(column).appendTo($(row));
    }

    var j=0;
    blogs.forEach(function(blog){
        var columnIndex = j%numberOfColumns;
        $(blog).appendTo($(columns[columnIndex]));
        j++;
    });

};

$(function(){
    setup_blogs_layout();
});