
// When the document is ready
$(document).ready(function () {
    setup_auto_font_resize();
    setup_nav_highlighting();
    setup_index_menu();
    setup_middle_navigation_highlighting();

    $(".alert-success").fadeTo(3000, 500).slideUp(500);

    $(".us-phone-mask").mask("(999) 999-9999");

});

var setup_auto_font_resize = function(){
    $('.profile-overview .title, .profile-overview .content, .profile-overview .cats span').quickfit(
        { max: 20, min: 9}
    );
};

var setup_index_menu = function(){

    $(".profile-overviews, .pagination-cards").hide();
    $(".profile-overviews#featured-gradline-section, #featured-gradline-pagination, #discover-previews-pagination").show("slow");

    setup_menu_item('#featured-gradline-item');
    setup_menu_item('#recently-added-item');
    setup_menu_item('#high-school-item');
    setup_menu_item('#college-item');
    setup_menu_item('#grad-school-item');
};

var setup_menu_item = function(id) {

    $(id).click(function(event){
        event.preventDefault();
        $("#index-menu li").removeClass('active');
        $(this).parent().addClass('active');

        $(".profile-overviews, .pagination-cards").hide("slow");

        var section_id = id.replace("item", "section");
        $(section_id).show("slow");

        var pagination_id = id.replace("item", "pagination");
        $(pagination_id).show("slow");
    });
};

var setup_nav_highlighting = function(){
    if (window.location.pathname.indexOf("faq") !== -1) {
        $("div.main-header-navbar#top-nav li.faq").addClass("nav-bar-active");
    }

    if (window.location.pathname.indexOf("blogs") !== -1) {
        $("div.main-header-navbar#top-nav li.blog").addClass("nav-bar-active");
    }

    if (window.location.pathname.indexOf("profile") !== -1
        || window.location.pathname.indexOf("login") !== -1) {
        $("div.main-header-navbar#top-nav li.login-me").addClass("nav-bar-active");
    }

    if (window.location.pathname.indexOf("register") !== -1) {
        $("div.main-header-navbar#top-nav li.logout-reg").addClass("nav-bar-active");
    }
};



var setup_middle_navigation_highlighting = function(){
    if (window.location.pathname.indexOf("profile/") !== -1) {
        $("ul.profile-nav li.profile-view").addClass("active");
    }

    if (window.location.pathname.indexOf("profile-edit/") !== -1) {
         $("ul.profile-nav li.profile-edit").addClass("active");
    }

    if (window.location.pathname.indexOf("gradlift-scholarship") !== -1) {
         $("ul.profile-nav li.gradlift-scholarship").addClass("active");
    }

    if (window.location.pathname.indexOf("account") !== -1) {
         $("ul.profile-nav li.account").addClass("active");
    }
};


$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
}); 
