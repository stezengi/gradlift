var http = {

    init: function () {
        var csrftoken = $.cookie('csrftoken');
        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $('.ajax-form').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);

            var success_function_name = $(this).attr('onsuccess');
            var success_function = eval(success_function_name);

            var error_function_name = $(this).attr('onerror');
            var error_function = eval(error_function_name);

            http.ajax_request(
                $(this).attr('method'),
                $(this).attr('action'),
                formData,
                function(data){
                    if (success_function != undefined) {
                        success_function(data);
                    }
                },
                function(data){
                    if (error_function != undefined) {
                        error_function(data);
                    }
            });
        }));
    },

    get: function(url, data, success_callback, error_callback) {
        this.ajax_request("GET", url, data, success_callback, error_callback);
    },

    post: function(url, data, success_callback, error_callback) {
        this.ajax_request("POST", url, data, success_callback, error_callback);
    },

    ajax_request: function(type, url, data, success_callback, error_callback) {

        $.ajax({
            type: type,
            url: "/ajax/" + url,
            data: data,
            success: success_callback,
            cache:false,
            contentType: false,
            processData: false,
            error: function (xhr, ajaxOptions, thrownError) {

                if(xhr.status == 403) {
                    window.location = "/login/";
                    return;
                }

                if (error_callback != undefined) {
                    error_callback(xhr, ajaxOptions, thrownError);
                }

            }
        });
    }
};

$(function(){
    http.init();
});
