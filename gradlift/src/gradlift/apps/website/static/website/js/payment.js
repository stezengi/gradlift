$(function() {

    $('input[name="selected-payment"]').change(function(){
        var value = $('input[name="selected-payment"]:checked').val();
        $('#payment-amount').val(value);
        if ($.isNumeric($(this).val())) {
            $("#invalid-amount-error").hide();
        } else {
            $("#invalid-amount-error").show();
        }
    });

    $("select#contribution-type").hide();
    $('#is-recurring').change(function(){
        var value = $(this).prop('checked');
        if (value) {
            $('div.toggle.btn').css("min-width", "120px");
            $("select#contribution-type").show();
        } else {
            $('div.toggle.btn').css("min-width", "150px");
            $("select#contribution-type").hide();
        }
    });

    $("button.support-button").click(function(e) {

        payment_additional_info.user_id = $(this).attr('data-student-user-id');
        payment_additional_info.name = $(this).attr('data-student-name');

        $("#supported-student-name").html(payment_additional_info.name);
    });

    $('#payment-amount').change(function(){
        if ($.isNumeric($(this).val())) {
            $("#invalid-amount-error").hide();
        } else {
            $("#invalid-amount-error").show();
        }
    });

    $('#start-payment-processing').on('click', function(e) {

        e.preventDefault();

        var amount = $('#payment-amount').val();
        if (!$.isNumeric(amount)) {
            $("#invalid-amount-error").show();
            return;
        }

        $('#payment-start-modal').modal('hide');

        payment_additional_info.amount = parseInt($('#payment-amount').val()) * 100;



        payment_additional_info.contribution_type = $('#contribution-type').val();
        payment_additional_info.is_recurring = $('#is-recurring').prop('checked');

        // Open Checkout with further options
        handler.open({
            name: 'GradLift, Inc.',
            description: '',
            amount: payment_additional_info.amount,
            billingAddress: true
        });


    });

    // Close Checkout on page navigation
    $(window).on('popstate', function() {
        handler.close();
    });

});

var send_payment = function(payment_data) {

    $.ajax({
        type: 'POST',
        url: "/ajax/make-payment/",
        data: {
            'client_ip': payment_data.client_ip,
            'stripe_token': payment_data.id,
            'created': payment_data.created,
            'email': payment_data.email,
            'address_city': payment_data.card.address_city,
            'address_country': payment_data.card.address_country,
            'address_street': payment_data.card.address_line1 + (payment_data.card.address_line2 == null ? "" : " " + payment_data.card.address_line2),
            'address_state': payment_data.card.address_state,
            'address_zip_code': payment_data.card.address_zip,
            'brand': payment_data.card.brand,
            'country': payment_data.card.country,
            'cvc_check': payment_data.card.cvc_check,
            'expiration_month': payment_data.card.exp_month,
            'expiration_year': payment_data.card.exp_year,
            'funding': payment_data.card.funding,
            'card_id': payment_data.card.id,
            'last_4_digits': payment_data.card.last4,
            'name': payment_data.card.name,
            'payment_additional_info_amount': payment_additional_info.amount,
            'payment_additional_info_contribution_type': payment_additional_info.contribution_type,
            'payment_additional_info_is_recurring': payment_additional_info.is_recurring,
            'payment_additional_info_user_id': payment_additional_info.user_id
        },
        success: function(data) {

            var title = "";
            var message = "";

            if (data.status == "success") {
                title = "Thank you for supporting " + data.student_name + "!";
                message = "Your card will be billed " + data.amount + "USD and will appear on your statment as: Gradlift, Inc."
            } else {
                title = "Error!";
                message = data.errors
            }

            BootstrapDialog.show({

                title: title,
                message: function(dialog) {
                    dialog.setClosable(false);
                    return message;
                },
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn-success',
                    action: function(dialog) {
                        location.reload();
                    }
                }]
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {

            BootstrapDialog.show({

                title: "Error!",
                message: function(dialog) {
                    dialog.setClosable(false);
                    return xhr.errors;
                },
                buttons: [{
                    label: 'Ok',
                    cssClass: 'btn-danger',
                    action: function(dialog) {
                        location.reload();
                    }
                }]
            });

        }
    });

};