
$(function(){

    if ((window.location.pathname.indexOf("account") !== -1) && (window.location.pathname.indexOf("general") !== -1)) {
        $(".um-account-side li a#account-general").addClass("current");
    }

    if ((window.location.pathname.indexOf("account") !== -1) && (window.location.pathname.indexOf("password-reset") !== -1)) {
        $(".um-account-side li a#account-password-reset").addClass("current");
    }

    if ((window.location.pathname.indexOf("account") !== -1) && (window.location.pathname.indexOf("funds-received") !== -1)) {
        $(".um-account-side li a#account-funds-received").addClass("current");
    }

    if ((window.location.pathname.indexOf("account") !== -1) && (window.location.pathname.indexOf("backed-students") !== -1)) {
        $(".um-account-side li a#account-backed-students").addClass("current");
    }

    if ((window.location.pathname.indexOf("account") !== -1) && (window.location.pathname.indexOf("disable-account") !== -1)) {
        $(".um-account-side li a#account-disable-account").addClass("current");
    }

    $('[data-toggle="tooltip"]').tooltip();

});
