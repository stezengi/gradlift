from django.conf.urls import url
from gradlift.apps.website.ajax import views

urlpatterns = [
    url(r'^upload-profile-picture/$', views.UploadProfilePictureView.as_view(), name='upload-profile-picture'),
    url(r'^upload-cover-image/$', views.UploadCoverImageView.as_view(), name='upload-cover-image'),
    url(r'^upload-loan-document/$', views.UploadLoanDocumentView.as_view(), name='upload-loan-document'),
    url(r'^make-payment/$', views.MakePaymentView.as_view(), name='make-payment'),
    url(r'^pagination-update/$', views.PaginationUpdateView.as_view(), name='pagination-page-update')
]