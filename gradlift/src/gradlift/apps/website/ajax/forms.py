import datetime

from django import forms
from django.conf import settings

from gradlift.apps.data import models
import stripe


class ProfilePictureForm(forms.Form):
    profile_picture = forms.ImageField(required=False)

    def save(self, user):
        user.profile.profile_picture = self.cleaned_data.get('profile_picture')
        user.profile.save()
        return user.profile.profile_picture_url


class CoverImageForm(forms.Form):
    cover_image = forms.ImageField(required=False)

    def save(self, user):
        user.profile.cover_image = self.cleaned_data.get('cover_image')
        user.profile.save()
        return user.profile.cover_image_url


class LoanDocumentForm(forms.Form):
    document = forms.FileField(required=False)

    def save(self, user):
        try:
            student_loan = models.StudentLoan.objects.get(student=user.profile.student)
        except models.StudentLoan.DoesNotExist:
            student_loan = models.StudentLoan(student=user.profile.student)

        student_loan.is_loan_statement_uploaded = True
        student_loan.save()
        notification_type = models.RefNotificationType.objects.get(
            code=models.RefNotificationType.TYPE_LOAN_DOCUMENTS_UPLOADED)

        models.Notification(type=notification_type,
                            student=user.profile.student).save()

        student_loan_document = models.StudentLoanDocument(student_loan=student_loan)

        student_loan_document.document = self.cleaned_data.get('document')
        student_loan_document.save()


class MakePaymentForm(forms.Form):

    client_ip = forms.GenericIPAddressField(required=False)
    stripe_token = forms.CharField(required=False)
    created = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    address_city = forms.CharField(required=False)
    address_country = forms.CharField(required=False)
    address_street = forms.CharField(required=False)
    address_state = forms.CharField(required=False)
    address_zip_code = forms.CharField(required=False)
    brand = forms.CharField(required=False)
    country = forms.CharField(required=False)
    cvc_check = forms.CharField(required=False)
    expiration_month = forms.IntegerField(required=False)
    expiration_year = forms.IntegerField(required=False)
    funding = forms.CharField(required=False)
    card_id = forms.CharField(required=False)
    last_4_digits = forms.CharField(required=False)
    name = forms.CharField(required=False)

    payment_additional_info_amount = forms.IntegerField()
    payment_additional_info_contribution_type = forms.CharField(required=False)
    payment_additional_info_is_recurring = forms.BooleanField(required=False)
    payment_additional_info_user_id = forms.IntegerField()

    def clean_created(self):
        created_in_unix_timestamp = self.cleaned_data.get('created')
        return datetime.datetime.fromtimestamp(int(created_in_unix_timestamp))

    def clean_payment_additional_info_contribution_type(self):
        if settings.IS_PAYMENT_RECURRING_ENABLED:
            payment_additional_info_contribution_type = self.cleaned_data.get('payment_additional_info_contribution_type')
            return models.RefContributionType.objects.get(code=payment_additional_info_contribution_type)

    def save(self, user):

        stripe_token = self.cleaned_data.get('stripe_token')

        if not user.is_anonymous():
            user_id = user.pk
            user = user
        else:
            user_id = None
            user = None

        try:
            payment = models.StripeCustomerPayment.objects.get(stripe_token=stripe_token)
            new_payment = False
        except models.StripeCustomerPayment.DoesNotExist:
            new_payment = True

        if new_payment:
            if user:
                try:
                    old_payment = models.StripeCustomerPayment.objects.get(user=user, is_most_recent=True)
                    old_payment.is_most_recent = False
                    old_payment.save()
                except models.StripeCustomerPayment.DoesNotExist:
                    pass

            if user:
                payment = models.StripeCustomerPayment(user=user)
            else:
                payment = models.StripeCustomerPayment()

            payment.client_ip = self.cleaned_data.get('client_ip')
            payment.stripe_token = self.cleaned_data.get('stripe_token')
            payment.created = self.cleaned_data.get('created')
            payment.email = self.cleaned_data.get('email')
            payment.address_city = self.cleaned_data.get('address_city')
            payment.address_country = self.cleaned_data.get('address_country')
            payment.address_street = self.cleaned_data.get('address_street')
            payment.address_state = self.cleaned_data.get('address_state')
            payment.address_zip_code = self.cleaned_data.get('address_zip_code')
            payment.brand = self.cleaned_data.get('brand')
            payment.country = self.cleaned_data.get('country')
            payment.cvc_check = self.cleaned_data.get('cvc_check')
            payment.expiration_month = self.cleaned_data.get('expiration_month')
            payment.expiration_year = self.cleaned_data.get('expiration_year')
            payment.funding = self.cleaned_data.get('funding')
            payment.card_id = self.cleaned_data.get('card_id')
            payment.last_4_digits = self.cleaned_data.get('last_4_digits')
            payment.name = self.cleaned_data.get('name')

            customer = stripe.Customer.create(card=stripe_token, description=payment.email, email=payment.email)
            payment.fingerprint = customer["sources"]["data"][0]["fingerprint"]
            payment.stripe_customer_id = customer["id"]
            payment.save()

        return models.Contribution.objects.make_contribution(
            student_user_id=self.cleaned_data.get('payment_additional_info_user_id'),
            amount=self.cleaned_data.get('payment_additional_info_amount'),
            stripe_customer_payment=payment,
            is_recurring=self.cleaned_data.get('payment_additional_info_is_recurring'),
            contribution_type=self.cleaned_data.get('payment_additional_info_contribution_type'),
            backer_user_id=user_id
        )