import json

from django.http import HttpResponse

from django.views.generic import View
from django.conf import settings
from django.core.cache import cache
from django.shortcuts import render_to_response

from gradlift.apps import Constants
from gradlift.apps.website.ajax import forms
from gradlift.apps.data import models

import stripe

stripe.api_key = settings.STRIPE_SECRET


class AjaxView(View):
    def response(self, context={}, status=200):
        response_json = json.dumps(context)
        return HttpResponse(response_json, content_type='application/json', status=status)

    def response_bad_request(self, context={}):
        return self.response(context=context, status=400)

    def response_forbidden(self, context={}):
        return self.response(context=context, status=403)


class AjaxLoginRequiredView(AjaxView):
    """
    View mixin which verifies that the user is authenticated.

    NOTE:
        This should be the left-most mixin of a view, except when
        combined with CsrfExemptMixin - which in that case should
        be the left-most mixin.
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return self.response_forbidden(context={'is_authenticated': False})

        return super(AjaxLoginRequiredView, self).dispatch(request, *args, **kwargs)

    def response(self, context={}, status=200):
        context.update({"is_authenticated": True})
        return super(AjaxLoginRequiredView, self).response(context=context, status=status)


class UploadProfilePictureView(AjaxLoginRequiredView):

    def post(self, request):
        form = forms.ProfilePictureForm(request.POST, request.FILES)

        if form.is_valid():
            new_image_url = form.save(user=request.user)

            return self.response(context={"success": True, "new_image_url": new_image_url})
        else:
            return self.response_bad_request(context=form.errors)


class UploadCoverImageView(AjaxLoginRequiredView):

    def post(self, request):
        form = forms.CoverImageForm(request.POST, request.FILES)

        if form.is_valid():
            new_image_url = form.save(user=request.user)

            return self.response(context={"success": True, "new_image_url": new_image_url})
        else:
            return self.response_bad_request(context=form.errors)


class UploadLoanDocumentView(AjaxLoginRequiredView):

    def post(self, request):
        form = forms.LoanDocumentForm(request.POST, request.FILES)

        if form.is_valid():
            form.save(user=request.user)

            return self.response(context={"status": 'success'})
        else:
            return self.response(context={"status": 'error'})


class MakePaymentView(AjaxView):

    def post(self, request):
        user = request.user
        form = forms.MakePaymentForm(request.POST)
        if form.is_valid():
            success, error_message = form.save(user=user)

            user_id = form.cleaned_data.get('payment_additional_info_user_id')
            student_user = models.User.objects.get(pk=user_id)
            student_name = student_user.get_full_name()

            if success:
                return self.response(context={
                    "status": 'success',
                    "amount": str(int(form.cleaned_data.get('payment_additional_info_amount'))/100) + ".00",
                    "student_name": student_name
                })
            else:
                return self.response(context={
                    "status": 'error',
                    "errors": error_message
                })
        else:
            return self.response(context={"status": 'error', 'errors': str(form.errors)})


class PaginationUpdateView(AjaxView):

    def get(self, request):
        page_number = int(request.GET.get('page_number'))
        pagination_page_id = request.GET.get('pagination_page_id')
        search_term = request.GET.get('search_term')

        cache_key = "pagination-update:%s:%s:%s" % (pagination_page_id, page_number, search_term,)
        profile_previews = cache.get(cache_key)

        if profile_previews is None:
            profile_previews = []
            if pagination_page_id == 'discover-previews-section':
                if len(search_term) > 0:
                    profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.discover_profile_overviews(
                        search_term=search_term,
                        page_number=page_number)
                else:
                    profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_all_profile_overviews(
                        page_number=page_number)
            elif pagination_page_id == 'featured-gradline-section':
                profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_featured_profile_overviews(
                    page_number=page_number)
            elif pagination_page_id == 'recently-added-section':
                profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_recently_added_profile_overviews(
                    page_number=page_number)
            elif pagination_page_id == 'high-school-section':
                profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_high_school_profile_overviews(
                    page_number=page_number)
            elif pagination_page_id == 'college-section':
                profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_college_profile_overviews(
                    page_number=page_number)
            elif pagination_page_id == 'grad-school-section':
                profile_previews, profile_overviews_max_page_number = models.UserProfile.objects.get_grad_school_profile_overviews(
                    page_number=page_number)

            cache.set(cache_key, profile_previews, Constants.CACHE_TIME_ONE_HOUR)

        return render_to_response('ajax-profile-previews.html', {'profile_overviews': profile_previews})