#!/usr/bin/env sh

sh /home/vagrant/virtualenv/bin/activate
export PYTHONPATH=/vagrant/gradlift/src/gradlift
/home/vagrant/virtualenv/bin/celery -A gradlift worker -l info --workdir=/vagrant/gradlift/src
